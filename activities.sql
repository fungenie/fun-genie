--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: activities_activity; Type: TABLE DATA; Schema: public; Owner: -
--

COPY activities_activity (id, name, url, acceptable_weather_conditions, unacceptable_clouds_code, min_wind_speed, max_wind_speed, min_temp, max_temp, start_time, end_time, weights) FROM stdin;
1	Visit an art museum	https://maps.google.com/maps?sll=%F&q=art+museum	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{5,5,0,3,6,7,7,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,4,7,NaN,NaN,NaN,0,10,4,NaN,NaN,7,NaN,3,4,0,10}
2	Play laser tag	https://maps.google.com/maps?sll=%F&q=laser+tag	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{8,2,0,5,8,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,4,NaN,6,8,NaN,NaN,2,0,10,6,NaN,5,NaN,NaN,7,2,0,10}
3	Play baseball, practice fielding and plays	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{10,0,0,10,3,0,NaN,NaN,NaN,10,0,NaN,NaN,0,10,NaN,0,7,NaN,NaN,NaN,10,0,6,4,7,3,0,10,2,0,10}
4	Play dominoes	https://www.google.com/search?q=how+to+play+dominoes	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,7,4,2,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,3,NaN,NaN,NaN,0,10,6,NaN,4,NaN,NaN,5,1,0,10}
5	Make a clay pot	https://www.google.com/search?q=model+with+clay	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,5,3,2,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,2,8,NaN,NaN,0,10,6,6,NaN,NaN,6,6,4,0,10}
6	Play a board game you haven't played in a long time	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,5,4,1,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,4,NaN,3,NaN,NaN,7,1,0,10}
7	Find a query that yields exactly one Google search result	http://google.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,2,6,2,NaN,5,NaN,NaN,NaN,3,NaN,3,6,NaN,0,10,NaN,NaN,NaN,0,10,8,4,2,8,7,3,1,10,0}
8	Read movie reviews	https://www.google.com/search?q=movie+reviews	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,4,2,NaN,NaN,NaN,NaN,2,2,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,0,10,9,1,3,3,10,0,1,5,5}
9	Learn programming	https://www.google.com/search?q=Learn+programming	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,3,NaN,8,NaN,NaN,NaN,8,NaN,NaN,4,NaN,1,8,NaN,NaN,NaN,0,10,7,NaN,NaN,8,NaN,2,10,0,10}
10	Go hiking	https://maps.google.com/maps?sll=%F&q=scenic+hike	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{8,2,0,0,8,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,3,NaN,9,NaN,10,0,NaN,6,NaN,NaN,NaN,4,4,0,10}
11	Take nature photography	https://maps.google.com/maps?sll=%F&q=take+nature+photography	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{2,8,0,1,3,6,5,3,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,2,7,3,8,NaN,10,0,NaN,7,NaN,5,5,NaN,3,0,10}
12	Polish your silver	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,0,0,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,0,3,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,9,1,2,0,10}
13	Learn about a different country	http://dir.yahoo.com/regional/countries/	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,6,2,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,NaN,4,NaN,NaN,NaN,0,10,8,3,NaN,3,10,0,2,5,5}
14	Tour the wine country	https://maps.google.com/maps?sll=%F&q=Tour+the+wine+country	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{3,7,0,0,6,7,NaN,NaN,NaN,8,2,1,NaN,NaN,NaN,3,4,NaN,NaN,6,NaN,8,2,8,3,6,NaN,1,9,4,0,10}
15	Play Scrabble	http://www.hasbro.com/scrabble/en_US/scrabbleGame.cfm	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,7,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,8,3,6,NaN,NaN,10,1,0,10}
16	Try new makeup	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{0,10,0,0,7,3,1,NaN,NaN,NaN,NaN,1,NaN,10,0,NaN,2,2,2,NaN,NaN,0,10,8,3,3,NaN,7,3,1,10,0}
17	Play a game of jacks	http://www.ehow.com/how_2964_play-jacks.html	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,10,0,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,5,5,9,1,2,NaN,NaN,10,1,0,10}
18	Read book reviews	https://www.google.com/search?q=book+reviews	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,5,6,2,NaN,NaN,NaN,NaN,2,2,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,0,10,9,1,3,3,10,0,1,5,5}
19	Make a quilt	https://www.google.com/search?q=how+to+quilt	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,2,8,5,NaN,NaN,NaN,NaN,NaN,NaN,8,0,NaN,2,2,10,NaN,NaN,0,10,6,6,3,NaN,8,3,10,0,10}
20	Work on a creative photo album	https://www.google.com/search?q=creative+photo+album	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,5,4,4,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,2,7,6,NaN,NaN,0,10,NaN,NaN,NaN,NaN,8,2,4,0,10}
21	Invent a secret language	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,7,4,0,NaN,NaN,NaN,NaN,NaN,1,NaN,NaN,NaN,NaN,NaN,3,6,NaN,NaN,NaN,NaN,NaN,8,8,NaN,5,5,3,0,10}
22	Knitting	https://www.google.com/search?q=how+to+knit	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,1,5,8,4,NaN,NaN,NaN,NaN,NaN,NaN,9,1,NaN,4,3,5,NaN,NaN,0,10,8,NaN,2,NaN,NaN,3,4,0,10}
23	Go for a walk and observe wildlife	https://maps.google.com/maps?sll=%F&q=scenic+hike	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{5,5,0,1,6,9,NaN,NaN,NaN,10,0,3,NaN,NaN,NaN,NaN,NaN,3,NaN,9,NaN,10,0,NaN,6,NaN,3,NaN,NaN,4,0,10}
24	Make a friendship bracelet	https://www.google.com/search?q=make+a+friendship+bracelet	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,8,2,0,5,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,0,NaN,8,NaN,NaN,0,10,6,6,NaN,NaN,3,10,2,0,10}
25	Have a movie marathon	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,2,7,3,NaN,NaN,NaN,NaN,5,1,10,NaN,NaN,NaN,1,7,NaN,NaN,NaN,0,10,8,NaN,5,5,NaN,10,6,10,0}
26	Invite friends over to watch TV	\N	\N	\N	NaN	NaN	NaN	NaN	SS+1	SS+3	{2,8,0,3,8,5,NaN,NaN,NaN,NaN,NaN,NaN,8,6,4,NaN,0,5,NaN,NaN,NaN,0,10,8,3,5,5,NaN,10,4,10,0}
27	Shop for new furniture	\N	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,7,2,1,NaN,NaN,NaN,NaN,NaN,NaN,7,2,NaN,8,3,NaN,0,4,0,10,9,1,NaN,NaN,8,3,3,0,10}
28	Put together a jigsaw puzzle	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,8,3,1,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,2,1,NaN,NaN,0,10,6,NaN,3,NaN,NaN,4,1,0,10}
29	Play cribbage	https://www.google.com/search?q=how+to+play+cribbage	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,2,4,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,6,NaN,NaN,NaN,0,10,6,NaN,NaN,5,NaN,7,1,0,10}
30	Build a sand castle in a playground or the beach	\N	n/a or 	\N	0	10	21	NaN	SR+3	SS-1	{2,8,0,10,0,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,3,NaN,NaN,10,0,6,6,NaN,NaN,8,3,1,0,10}
31	Make a present for a friend	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{3,7,0,2,5,3,5,NaN,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,6,7,NaN,NaN,0,10,NaN,NaN,NaN,NaN,9,3,6,0,10}
32	Make a flip book	http://www.youtube.com/watch?v=iExiCGV7jzI	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,5,3,2,3,1,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,7,7,NaN,NaN,0,10,NaN,NaN,NaN,NaN,8,3,5,0,10}
33	Play poker with friends	https://www.google.com/search?q=how+to+play+poker	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,0,4,5,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,7,NaN,0,6,NaN,NaN,NaN,0,10,4,NaN,NaN,3,NaN,7,2,0,10}
34	Play chess	https://maps.google.com/maps?sll=%F&q=chess+club	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,1,5,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,8,NaN,NaN,NaN,0,10,4,NaN,NaN,3,NaN,7,2,0,10}
35	Play Apples to Apples board game	http://en.wikipedia.org/wiki/Apples_to_Apples	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,4,4,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,1,NaN,NaN,NaN,0,10,8,3,7,NaN,NaN,10,1,0,10}
36	Go swimming	https://maps.google.com/maps?sll=%F&q=swimming+pool	n/a or 	\N	0	10	21	NaN	SR+1	SS+4	{8,2,0,4,5,2,NaN,NaN,NaN,7,3,NaN,NaN,NaN,NaN,NaN,1,8,NaN,NaN,NaN,7,3,8,3,3,5,3,5,2,0,10}
37	Volunteer at a school	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{3,7,0,0,7,4,NaN,1,7,8,3,3,NaN,NaN,NaN,NaN,0,3,NaN,NaN,NaN,0,10,8,3,4,2,NaN,10,4,0,10}
38	Go to Popular Science and find interesting science articles	http://www.popsci.com/archives	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,5,10,NaN,10,NaN,NaN,NaN,8,NaN,5,5,NaN,0,10,NaN,NaN,NaN,0,10,10,0,0,10,7,3,1,10,0}
39	Play jumprope	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{8,2,0,10,0,0,NaN,NaN,NaN,10,0,NaN,NaN,8,2,NaN,0,8,NaN,NaN,NaN,10,0,4,NaN,3,NaN,NaN,7,1,0,10}
40	Create a flower arrangement	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,3,7,4,NaN,NaN,5,NaN,NaN,NaN,9,0,NaN,2,4,7,5,NaN,0,10,NaN,NaN,NaN,2,9,2,2,0,10}
41	Go to a boutique store	https://maps.google.com/maps?sll=%F&q=boutique	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,7,7,NaN,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,0,4,0,10,9,1,NaN,NaN,8,3,2,0,10}
42	Challenge someone to arm wrestle	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{10,0,8,3,6,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,NaN,NaN,0,10,8,2,3,7,NaN,10,1,0,10}
43	Build a house of cards	http://www.youtube.com/watch?v=XBEFOEnwRk8	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,4,2,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,5,1,NaN,NaN,0,10,8,NaN,NaN,3,7,NaN,1,0,10}
44	Read a new magazine	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,8,8,NaN,NaN,NaN,NaN,NaN,8,8,NaN,NaN,NaN,1,7,NaN,NaN,NaN,0,10,NaN,NaN,NaN,NaN,9,NaN,1,0,10}
45	Go fishing	https://maps.google.com/maps?sll=%F&q=fishing	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{5,5,7,1,4,7,NaN,NaN,NaN,10,0,NaN,NaN,NaN,8,NaN,2,NaN,NaN,10,NaN,10,0,8,3,3,3,8,3,4,0,10}
46	Make origami	https://www.google.com/search?q=how+to+make+origami	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,1,4,2,5,NaN,NaN,NaN,NaN,1,NaN,7,3,NaN,1,6,4,NaN,NaN,0,10,7,NaN,NaN,NaN,5,NaN,2,0,10}
47	Play bridge online	https://www.google.com/search?q=play+bridge+online	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,6,6,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,NaN,0,6,NaN,NaN,NaN,0,10,4,NaN,NaN,3,9,NaN,2,10,0}
48	Work in a community garden	https://maps.google.com/maps?sll=%F&q=community+garden	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{3,7,0,0,3,1,NaN,NaN,NaN,10,0,1,NaN,3,NaN,NaN,0,2,NaN,NaN,NaN,10,0,9,1,NaN,NaN,5,5,3,0,10}
49	Learn a new language	https://www.google.com/search?q=learn+a+new+language	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,2,7,3,NaN,NaN,NaN,NaN,NaN,10,NaN,NaN,NaN,NaN,0,7,NaN,NaN,NaN,0,10,10,NaN,NaN,5,3,7,10,10,0}
50	Play marbles	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{1,9,0,10,0,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,1,2,NaN,NaN,NaN,5,5,4,NaN,3,NaN,NaN,7,1,0,10}
51	Invite friends over for coffee	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SR+6	{2,8,0,0,6,9,NaN,NaN,NaN,NaN,NaN,NaN,NaN,7,3,6,2,NaN,NaN,NaN,NaN,0,10,8,3,7,NaN,NaN,10,2,0,10}
52	Try pearl tea	https://maps.google.com/maps?sll=%F&q=pearl+tea	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,1,7,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,6,1,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,8,3,1,10,0}
53	Learn a new musical instrument	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,2,9,9,10,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,5,10,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,4,8,2,8,0,10}
54	Cook for a school event	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,7,2,NaN,NaN,9,8,NaN,NaN,NaN,7,NaN,9,4,4,5,NaN,NaN,0,10,4,NaN,NaN,5,3,NaN,3,0,10}
55	Make a pillow fort	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{3,7,0,10,0,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,10,8,NaN,NaN,0,10,5,NaN,10,NaN,NaN,8,1,0,10}
56	Volunteer at a soup kitchen	https://maps.google.com/maps?sll=%F&q=soup+kitchen	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{3,7,0,0,3,2,NaN,NaN,10,NaN,NaN,2,NaN,NaN,NaN,5,1,3,NaN,NaN,5,0,10,7,NaN,5,NaN,NaN,7,6,0,10}
57	Go birdwatching	https://maps.google.com/maps?sll=%F&q=birdwatching	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{3,7,0,0,3,8,NaN,NaN,NaN,10,0,5,NaN,NaN,NaN,NaN,1,6,NaN,10,NaN,10,0,7,NaN,NaN,NaN,NaN,NaN,3,0,10}
58	Go backcountry snowshoeing	\N	\N	\N	0	10	NaN	5	SR+3	SS-1	{8,2,0,3,8,4,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,2,3,NaN,10,NaN,10,0,8,3,NaN,NaN,3,8,4,0,10}
59	Learn sign language	https://www.google.com/search?q=Learn+sign+language	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,3,4,2,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,0,7,NaN,NaN,NaN,0,10,10,NaN,NaN,5,3,7,10,10,0}
60	Play Frisbee Golf	https://www.google.com/search?q=how+to+play+Frisbee+Golf	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{8,2,0,1,7,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,5,NaN,NaN,NaN,8,2,6,NaN,5,NaN,NaN,6,3,0,10}
61	Take a crafts class	https://www.google.com/search?q=crafts+class	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,5,3,3,4,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,3,5,4,NaN,NaN,0,10,9,1,3,3,1,9,10,10,0}
62	Take art photography	https://www.google.com/search?q=how+to+take+art+photography	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,1,4,4,7,0,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,2,7,3,NaN,NaN,NaN,NaN,NaN,8,NaN,5,5,NaN,3,0,10}
63	Paint a chair a bright color	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,5,2,3,NaN,NaN,NaN,NaN,NaN,NaN,9,3,NaN,2,2,NaN,NaN,NaN,0,10,8,3,NaN,NaN,10,0,4,0,10}
64	Go to a music store	https://maps.google.com/maps?sll=%F&q=music+store	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,1,7,3,1,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,0,10,8,3,NaN,NaN,8,3,2,0,10}
65	Write a blog	https://www.google.com/search?q=how+to+write+a+blog	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,3,1,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,5,NaN,NaN,NaN,0,10,3,8,1,4,10,0,4,0,10}
66	Try an experiment from Science Bob	http://www.sciencebob.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{3,7,0,3,5,8,NaN,10,NaN,8,NaN,8,NaN,3,6,NaN,3,7,5,NaN,NaN,0,10,8,4,2,5,NaN,NaN,2,0,10}
67	Play with a random phrase generator	http://www.manythings.org/rs/	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,5,8,NaN,5,NaN,NaN,NaN,3,NaN,3,6,NaN,0,4,NaN,NaN,NaN,0,10,8,4,10,0,7,3,1,10,0}
68	Read a comic book	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,10,5,0,2,NaN,NaN,NaN,NaN,1,6,NaN,NaN,NaN,1,6,NaN,NaN,NaN,0,10,4,NaN,3,NaN,10,0,2,0,10}
69	Play football plays	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{10,0,0,10,3,0,NaN,NaN,NaN,10,0,NaN,NaN,0,10,NaN,0,7,NaN,NaN,NaN,10,0,6,4,7,3,0,10,2,0,10}
70	Learn to cook a new recipe	https://www.google.com/search?q=new+recipe	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{4,6,0,2,3,1,NaN,NaN,NaN,6,NaN,7,NaN,3,6,9,2,5,3,NaN,NaN,0,10,4,NaN,NaN,4,NaN,NaN,4,0,10}
71	Sell unwanted stuff online	http://www.craigslist.org/about/sites/	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,7,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,8,3,NaN,NaN,10,0,3,10,0}
72	Listen to the radio	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,4,8,NaN,NaN,NaN,NaN,NaN,3,5,NaN,NaN,NaN,0,1,NaN,NaN,NaN,0,10,6,NaN,NaN,NaN,5,NaN,3,10,0}
73	Solve a crossword puzzle	https://www.google.com/search?q=crossword+puzzles	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,5,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,8,NaN,NaN,NaN,9,NaN,1,0,10}
74	Go to a coffeeshop	https://maps.google.com/maps?sll=%F&q=coffeeshop	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,7,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,3,1,1,NaN,NaN,5,0,10,3,NaN,NaN,NaN,NaN,6,2,0,10}
75	Create a crossword puzzle	https://www.google.com/search?q=create+crossword+puzzles	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,5,8,NaN,NaN,NaN,NaN,NaN,1,NaN,NaN,NaN,NaN,NaN,8,5,NaN,NaN,0,10,NaN,4,NaN,NaN,6,NaN,4,0,10}
76	Go to a plant nursery	https://maps.google.com/maps?sll=%F&q=plant+nursery	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{2,8,0,1,8,7,NaN,NaN,NaN,10,0,1,NaN,7,3,NaN,3,2,NaN,NaN,NaN,8,2,9,1,NaN,NaN,8,3,2,10,0}
77	Learn math	https://www.google.com/search?q=learn+math	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,4,2,NaN,8,NaN,NaN,NaN,8,NaN,NaN,4,NaN,1,8,NaN,NaN,NaN,0,10,7,NaN,NaN,8,NaN,2,10,0,10}
78	Try PhoneSpell with your phone number	http://www.phonespell.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,6,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,3,6,NaN,0,5,NaN,NaN,NaN,0,10,10,0,8,2,7,3,1,10,0}
79	Go fly a kite	https://maps.google.com/maps?sll=%F&q=bike+trail	n/a or 	\N	4	15	10	NaN	SR+3	SS-1	{6,4,0,10,4,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,6,NaN,10,0,NaN,6,4,NaN,NaN,NaN,2,0,10}
80	Research your family tree	https://www.google.com/search?q=research+family+tree	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,3,8,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,0,10,5,NaN,NaN,9,7,NaN,4,10,0}
81	Play gin rummy online	https://www.google.com/search?q=play+gin+rummy+online	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,4,2,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,NaN,0,6,NaN,NaN,NaN,0,10,4,NaN,3,NaN,9,NaN,2,10,0}
82	Nap in a hammock	\N	n/a or 	\N	0	10	21	NaN	SR+3	SS-1	{0,10,0,0,2,6,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,1,NaN,10,0,10,NaN,NaN,NaN,10,0,1,0,10}
83	Go bowling	https://maps.google.com/maps?sll=%F&q=bowling	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{3,7,0,1,5,7,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,6,5,NaN,NaN,2,0,10,6,NaN,5,NaN,NaN,7,2,0,10}
84	Go online and read the news	http://news.google.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,4,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,0,10,NaN,4,NaN,3,10,0,1,10,0}
85	Learn magic tricks	https://www.google.com/search?q=magic+tricks	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,6,4,1,NaN,NaN,NaN,6,NaN,8,NaN,NaN,NaN,NaN,0,5,NaN,NaN,NaN,0,10,8,3,NaN,7,8,3,8,0,10}
86	Talk to a homeless person	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{3,7,0,1,4,1,NaN,NaN,6,6,NaN,2,NaN,NaN,NaN,NaN,NaN,6,NaN,NaN,6,10,0,NaN,6,NaN,5,0,10,1,10,0}
87	Go for a hike and take photographs	https://maps.google.com/maps?sll=%F&q=scenic+hike	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{7,3,0,0,4,7,NaN,NaN,NaN,10,0,2,NaN,NaN,NaN,NaN,NaN,3,NaN,8,NaN,10,0,8,3,NaN,NaN,8,3,3,0,10}
88	Make holiday ornaments	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,3,3,1,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,2,NaN,NaN,NaN,0,10,6,6,NaN,NaN,6,6,2,0,10}
89	Try a new recipe	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{2,8,0,0,7,4,NaN,NaN,NaN,5,NaN,NaN,NaN,7,NaN,9,2,4,4,NaN,NaN,0,10,5,6,NaN,3,6,NaN,4,0,10}
90	Paint a mural	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,3,2,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,3,7,10,NaN,NaN,0,10,3,8,NaN,NaN,10,0,10,0,10}
91	Go to a restaurant with friends	https://maps.google.com/maps?sll=%F&q=restaurant	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,1,9,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,8,7,1,NaN,NaN,3,3,7,NaN,4,NaN,NaN,NaN,9,3,0,10}
92	Listen to music	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,6,8,3,NaN,NaN,NaN,NaN,3,7,NaN,NaN,NaN,0,1,NaN,NaN,NaN,0,10,4,NaN,NaN,NaN,7,NaN,3,10,0}
93	Build a model airplane	https://www.google.com/search?q=model+building+kits+%20adult	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,0,8,8,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,7,NaN,3,6,5,NaN,NaN,0,10,8,NaN,3,NaN,NaN,4,8,0,10}
94	Go for a bike ride	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS	{8,2,0,3,6,3,NaN,NaN,NaN,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,4,NaN,NaN,NaN,10,0,5,5,2,2,5,NaN,2,0,10}
95	Hunt for treasure with a metal detector	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{4,6,0,2,4,2,NaN,2,NaN,10,0,4,NaN,NaN,7,NaN,6,1,NaN,6,NaN,10,0,8,3,NaN,5,9,1,3,0,10}
96	Visit a book store	https://maps.google.com/maps?sll=%F&q=book+store	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,4,5,3,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,4,0,10,9,1,NaN,NaN,6,6,1,0,10}
97	Post on Facebook	http://www.facebook.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,9,6,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,3,NaN,NaN,NaN,0,10,8,3,NaN,NaN,10,0,1,10,0}
98	Give a friend a special recipe	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{1,9,0,0,2,8,NaN,NaN,7,NaN,NaN,1,NaN,10,0,3,NaN,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,NaN,10,1,0,10}
99	Hunt for interesting rocks	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{4,6,0,3,5,2,NaN,2,NaN,10,0,4,NaN,NaN,7,NaN,0,1,NaN,8,NaN,10,0,3,8,NaN,5,8,3,3,0,10}
100	Go visit the mall	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,7,7,NaN,NaN,NaN,NaN,NaN,1,NaN,9,0,NaN,1,2,NaN,NaN,7,2,8,3,NaN,NaN,NaN,NaN,5,2,0,10}
101	Attend a music performance	https://maps.google.com/maps?sll=%F&q=live+music	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,1,6,8,7,NaN,NaN,6,8,4,8,NaN,NaN,NaN,3,8,NaN,NaN,NaN,0,10,9,1,NaN,3,1,9,3,0,10}
102	Organize a garage sale	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{3,7,0,0,7,2,NaN,NaN,NaN,5,NaN,NaN,NaN,NaN,NaN,NaN,1,3,NaN,NaN,2,NaN,NaN,8,NaN,NaN,NaN,NaN,NaN,4,0,10}
103	Frame an old picture	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,2,8,3,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,2,2,NaN,NaN,NaN,0,10,8,3,NaN,NaN,10,0,2,0,10}
104	Paint your nails	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,1,10,6,2,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,1,2,3,NaN,NaN,0,10,8,3,NaN,NaN,6,4,1,0,10}
105	Play volleyball	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{10,0,0,2,10,7,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,NaN,6,NaN,NaN,NaN,8,2,5,NaN,2,NaN,NaN,10,2,0,10}
106	Have a picnic	https://maps.google.com/maps?sll=%F&q=picnic	n/a or 	\N	0	10	21	NaN	SR+3	SS-1	{3,7,0,8,7,4,NaN,NaN,NaN,10,0,NaN,NaN,7,5,10,4,1,NaN,7,NaN,10,0,NaN,4,NaN,NaN,NaN,8,3,0,10}
107	Go to an antique store	https://maps.google.com/maps?sll=%F&q=antique+store	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,2,8,NaN,NaN,NaN,NaN,NaN,1,NaN,9,1,NaN,3,NaN,NaN,NaN,NaN,0,10,9,1,NaN,NaN,8,3,2,0,10}
108	Go stargazing	\N	n/a or 	not OVC	0	10	10	NaN	SS+1	SR-1	{2,8,0,1,6,3,NaN,3,NaN,0,10,3,NaN,NaN,NaN,NaN,NaN,4,NaN,10,NaN,10,0,NaN,5,1,2,NaN,NaN,1,0,10}
109	Play charades with friends	http://en.wikipedia.org/wiki/Charades	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,5,4,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,8,3,8,3,NaN,10,2,0,10}
110	Draw a cartoon	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,0,5,4,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,6,6,NaN,NaN,0,10,NaN,8,2,NaN,7,NaN,4,0,10}
111	Read articles about the Pi Code	https://www.google.com/search?q=Pi+Code	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,6,10,NaN,5,NaN,NaN,NaN,8,NaN,3,6,NaN,0,10,NaN,NaN,NaN,0,10,10,0,0,10,7,3,1,10,0}
112	Go canoeing	https://maps.google.com/maps?sll=%F&q=canoeing	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{6,4,0,2,8,4,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,2,2,NaN,10,NaN,10,0,8,3,NaN,NaN,NaN,10,4,0,10}
113	Try an experiment from Science Is Fun	http://www.scifun.org	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{3,7,0,3,5,8,NaN,10,NaN,8,NaN,8,NaN,3,6,NaN,3,7,5,NaN,NaN,0,10,8,4,2,5,NaN,NaN,2,0,10}
114	Take a yoga or Pilates class	https://maps.google.com/maps?sll=%F&q=yoga+or+pilates+classes	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{4,6,0,1,5,7,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,3,4,NaN,NaN,NaN,0,10,9,1,3,NaN,1,9,10,0,10}
115	Play basketball	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{10,0,0,3,7,1,NaN,NaN,NaN,7,NaN,NaN,NaN,0,9,NaN,0,10,NaN,NaN,NaN,5,5,4,NaN,4,NaN,NaN,10,2,0,10}
116	Go for a run	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{10,0,0,0,6,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,9,NaN,NaN,NaN,10,0,5,NaN,NaN,6,5,NaN,1,0,10}
117	Make a treasure hunt	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,1,4,1,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,4,NaN,NaN,NaN,5,5,NaN,NaN,NaN,NaN,NaN,NaN,5,0,10}
118	Go to a home improvement store	https://maps.google.com/maps?sll=%F&q=home+improvement	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,5,5,NaN,NaN,NaN,7,NaN,1,NaN,5,NaN,NaN,4,2,NaN,NaN,NaN,3,7,9,1,NaN,NaN,8,3,2,10,0}
119	Make 5 new connections in LinkedIn	http://www.LinkedIn.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,9,1,NaN,NaN,10,0,1,10,0}
120	Plant a vegetable garden	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{4,6,0,1,7,6,NaN,NaN,NaN,10,0,2,NaN,5,NaN,NaN,2,2,NaN,NaN,NaN,10,0,9,1,NaN,NaN,8,3,4,0,10}
121	Make a new tablecloth	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,0,2,8,5,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,2,2,8,NaN,NaN,0,10,6,6,NaN,NaN,9,1,8,0,10}
122	Go to the beach	\N	n/a or 	\N	0	10	21	NaN	SR+3	SS-1	{5,5,0,6,7,3,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,6,1,NaN,9,NaN,10,0,7,NaN,7,NaN,NaN,6,6,0,10}
123	Go online and play games	https://www.google.com/search?q=online+games	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,1,9,5,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,0,10,6,NaN,NaN,NaN,9,NaN,3,10,0}
124	Play with a YoYo	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,8,4,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,3,7,9,1,NaN,NaN,10,0,1,0,10}
125	Read new recipes in print	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,6,8,NaN,NaN,NaN,NaN,NaN,3,NaN,7,NaN,9,0,3,NaN,NaN,NaN,0,10,NaN,7,NaN,NaN,7,NaN,3,0,10}
126	Go to a theater to see a movie	https://maps.google.com/maps?sll=%F&q=movie+cinema	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,4,9,6,1,NaN,NaN,NaN,NaN,1,6,NaN,NaN,NaN,3,2,NaN,NaN,2,0,10,7,NaN,3,NaN,NaN,6,3,10,0}
127	Play bridge with friends	https://maps.google.com/maps?sll=%F&q=bridge+club	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,0,4,9,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,NaN,0,6,NaN,NaN,NaN,0,10,4,NaN,NaN,3,NaN,7,2,0,10}
128	Make cookies for neighbors	http://www.epicurious.com/tools/browseresults?type=browse&att=31	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,1,7,7,1,NaN,7,NaN,NaN,NaN,NaN,9,3,10,3,2,3,NaN,NaN,0,10,8,NaN,6,NaN,NaN,6,3,0,10}
129	Visit historical sites of your city	https://maps.google.com/maps?sll=%F&q=historical+site	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{5,5,0,3,5,6,NaN,NaN,NaN,8,NaN,7,NaN,NaN,NaN,NaN,1,6,NaN,NaN,8,0,10,7,NaN,NaN,6,NaN,NaN,4,0,10}
130	Paint a picture by numbers	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,5,2,1,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,2,4,NaN,NaN,0,10,8,NaN,5,NaN,8,0,3,0,10}
131	Play Go online	https://www.google.com/search?q=play+Go+online	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,1,5,2,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,6,NaN,0,8,NaN,NaN,NaN,0,10,4,NaN,NaN,3,9,NaN,2,10,0}
132	Play checkers with a friend	https://www.google.com/search?q=how+to+play+checkers	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,2,4,6,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,4,NaN,3,NaN,NaN,7,1,0,10}
133	Go to First Sounds to hear early recordings	http://www.firstsounds.org	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,5,10,NaN,10,NaN,NaN,NaN,8,NaN,5,5,NaN,0,10,NaN,NaN,NaN,0,10,10,0,0,10,7,3,1,10,0}
134	Play badminton	\N	n/a or 	\N	0	10	21	NaN	SR+3	SS-1	{6,4,0,3,5,2,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,10,NaN,NaN,NaN,10,0,4,NaN,4,NaN,NaN,10,3,0,10}
135	Play tennis	https://maps.google.com/maps?sll=%F&q=tennis+court	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{10,0,0,2,4,1,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,NaN,NaN,1,10,NaN,NaN,NaN,7,3,4,NaN,4,NaN,NaN,3,2,0,10}
136	Visit the zoo	https://maps.google.com/maps?sll=%F&q=zoo	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{5,5,0,8,8,8,NaN,5,NaN,10,NaN,8,NaN,NaN,NaN,NaN,3,8,NaN,NaN,NaN,10,0,6,4,5,5,4,8,4,0,10}
137	Play an online game and watch TV… at same time	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,1,6,2,NaN,NaN,NaN,NaN,NaN,NaN,10,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,9,1,NaN,NaN,10,0,1,10,0}
138	Read a nonfiction book	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,2,6,7,3,NaN,NaN,NaN,NaN,7,5,NaN,NaN,NaN,1,7,NaN,NaN,NaN,0,10,4,NaN,NaN,3,10,0,3,0,10}
139	Go to a hobby shop	https://maps.google.com/maps?sll=%F&q=hobby+shop	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,10,5,5,NaN,NaN,NaN,10,0,5,5,3,7,NaN,2,4,2,NaN,2,0,10,4,NaN,3,3,5,NaN,2,0,10}
140	Plan a remodelling project	https://www.google.com/search?q=how+to+plan+a+remodelling+project	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,2,8,3,NaN,NaN,NaN,NaN,2,NaN,7,5,NaN,0,2,NaN,NaN,NaN,0,10,3,8,NaN,NaN,7,3,4,10,0}
141	Go online and use Twitter	http://twitter.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,8,4,NaN,NaN,NaN,NaN,NaN,6,NaN,NaN,NaN,NaN,0,3,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,10,0,1,10,0}
142	Teach someone to cook	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,6,3,NaN,NaN,4,7,NaN,NaN,NaN,7,NaN,9,2,5,3,NaN,NaN,0,10,NaN,NaN,NaN,5,6,6,4,0,10}
143	Regift an old necklace	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{0,10,0,0,2,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,0,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,3,8,1,0,10}
144	Draw a picture	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,7,4,8,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,8,7,NaN,NaN,0,10,NaN,8,NaN,4,7,NaN,1,0,10}
145	Play golf	https://maps.google.com/maps?sll=%F&q=golf	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{4,6,0,0,3,8,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,2,4,NaN,3,NaN,10,0,8,3,3,6,NaN,10,4,0,10}
146	Learn karate	https://www.google.com/search?q=karate	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{10,0,10,8,8,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,8,2,NaN,10,2,8,6,0,10}
147	Paint an original painting	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,2,5,7,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,8,10,NaN,NaN,0,10,NaN,10,NaN,8,8,0,10,0,10}
148	Spin a top	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,10,0,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,8,3,1,0,10}
149	Play the basketball game HORSE	http://en.wikipedia.org/wiki/Variations_of_basketball#H-O-R-S-E	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{7,3,0,3,7,1,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,7,NaN,0,8,NaN,NaN,NaN,5,5,4,NaN,4,NaN,NaN,8,1,0,10}
150	Plan and make a nice meal	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{3,7,0,0,8,6,NaN,NaN,5,NaN,NaN,NaN,NaN,10,5,10,6,3,4,NaN,NaN,0,10,6,NaN,NaN,NaN,NaN,4,2,0,10}
151	Make pasta	https://www.google.com/search?q=how+to+make+pasta	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS-1	{2,8,0,0,4,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,7,1,1,5,NaN,NaN,0,10,9,1,NaN,NaN,6,6,2,0,10}
152	Play Ultimate Frisbee	https://www.google.com/search?q=how+to+play+Ultimate+Frisbee	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{6,4,0,0,4,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,8,2,6,NaN,5,NaN,NaN,5,3,0,10}
153	Build and fly a glider	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{2,8,0,10,5,2,NaN,NaN,NaN,10,0,2,NaN,NaN,7,NaN,1,4,4,NaN,NaN,10,0,8,NaN,4,4,8,3,3,0,10}
154	Bake a loaf of bread	https://www.google.com/search?q=bread+recipes	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,1,6,7,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,8,1,1,5,NaN,NaN,0,10,9,1,NaN,NaN,9,1,2,0,10}
155	Explore books on Amazon	http://www.amazon.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,8,6,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,0,10,9,1,NaN,NaN,10,0,1,10,0}
156	Go to the gym	https://maps.google.com/maps?sll=%F&q=gym	\N	\N	NaN	NaN	NaN	NaN	SR	SS+4	{10,0,0,0,8,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,6,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,2,NaN,2,0,10}
157	Go for a nature trail run	https://maps.google.com/maps?sll=%F&q=scenic+hike	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{10,0,0,0,7,3,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,NaN,8,NaN,8,NaN,10,0,8,3,NaN,NaN,8,3,2,0,10}
158	Create a board game	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,1,7,4,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,7,7,NaN,NaN,0,10,NaN,8,NaN,NaN,6,NaN,8,0,10}
159	Go to a park	https://maps.google.com/maps?sll=%F&q=community+park	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{4,6,0,9,3,3,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,NaN,1,NaN,7,NaN,10,0,NaN,4,NaN,NaN,NaN,NaN,2,0,10}
160	Make homemade candles	https://www.google.com/search?q=make+homemade+candles	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,2,2,0,4,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,1,2,8,NaN,NaN,0,10,8,3,NaN,NaN,8,3,2,0,10}
161	Take a pet for a walk	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS+4	{3,7,0,8,5,6,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,6,NaN,10,0,8,3,6,NaN,8,2,1,10,0}
162	Test drive a new car	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,8,4,NaN,NaN,NaN,NaN,NaN,1,NaN,NaN,NaN,NaN,0,2,NaN,NaN,NaN,0,10,8,3,3,5,6,6,1,0,10}
163	Downhill ski	https://maps.google.com/maps?sll=%F&q=ski+resort	\N	\N	0	10	NaN	5	SR+3	SS-1	{10,0,0,2,8,4,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,3,8,NaN,10,NaN,10,0,8,3,NaN,NaN,3,8,4,0,10}
164	Learn Tai Chi	https://www.google.com/search?q=Learn+Tai+Chi	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{10,0,0,0,2,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,10,8,2,NaN,10,2,6,4,0,10}
165	Learn a YoYo trick	https://www.google.com/search?q=learn+a+new+yo+yo+trick	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,6,3,0,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,0,10,8,3,NaN,NaN,10,0,1,0,10}
167	Shop for a new rug	\N	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,7,2,1,NaN,NaN,NaN,NaN,NaN,NaN,9,1,NaN,6,3,NaN,0,4,0,10,9,1,NaN,NaN,8,3,3,0,10}
168	Go to a live theater performance	https://maps.google.com/maps?sll=%F&q=live+theater	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,0,8,3,3,NaN,NaN,NaN,NaN,2,8,NaN,NaN,NaN,4,7,NaN,NaN,NaN,5,5,8,3,NaN,NaN,6,6,3,0,10}
169	Visit a science museum	https://maps.google.com/maps?sll=%F&q=science+museum	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{5,5,0,5,5,2,NaN,8,NaN,NaN,NaN,8,NaN,NaN,4,NaN,4,7,NaN,NaN,NaN,0,10,4,NaN,NaN,7,NaN,3,4,0,10}
170	Go to real estate open houses	https://maps.google.com/maps?sll=%F&q=real+estate+open+house	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,7,4,NaN,NaN,NaN,10,0,NaN,NaN,7,3,NaN,NaN,3,NaN,NaN,5,3,7,4,NaN,NaN,NaN,NaN,NaN,2,0,10}
171	Build a Lego city	https://www.google.com/search?q=model+building+kits+%20beginner	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,2,6,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,2,3,NaN,NaN,0,10,8,NaN,4,NaN,NaN,4,4,0,10}
172	Make a handmade greeting card	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,1,3,2,5,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,1,NaN,4,NaN,NaN,0,10,NaN,NaN,NaN,NaN,NaN,NaN,2,0,10}
173	Go online and read random wiki articles	http://en.wikipedia.org/wiki/Main_Page	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,5,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,0,10,NaN,6,NaN,3,10,0,2,10,0}
174	Take a geography quiz	https://www.google.com/search?q=geography+quiz	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,1,2,0,NaN,NaN,NaN,NaN,NaN,6,NaN,NaN,NaN,NaN,NaN,6,NaN,NaN,NaN,0,10,10,NaN,NaN,3,10,0,1,10,0}
176	Give a friend a balloon bouquet	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{1,9,0,1,7,4,1,NaN,7,NaN,NaN,NaN,NaN,10,0,NaN,2,2,5,NaN,NaN,0,10,8,3,5,NaN,3,8,1,0,10}
177	Go camping	https://maps.google.com/maps?sll=%F&q=campground	n/a or 	\N	0	10	10	NaN	\N	\N	{8,2,0,1,7,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,10,NaN,10,0,NaN,7,NaN,NaN,NaN,8,10,0,10}
178	Make paper airplanes	https://www.google.com/search?q=how+to+make+paper+airplanes	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,8,4,2,NaN,1,NaN,5,NaN,1,NaN,NaN,NaN,NaN,0,3,2,NaN,NaN,5,5,5,NaN,NaN,NaN,5,NaN,2,0,10}
179	Shop for shoes online	https://www.google.com/search?q=shop+for+shoes+online	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,1,NaN,NaN,NaN,NaN,NaN,NaN,NaN,9,1,NaN,4,2,NaN,NaN,NaN,0,10,9,1,NaN,NaN,10,0,1,10,0}
180	Do embroidery	https://www.google.com/search?q=learn+embroidery	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,0,3,8,4,NaN,NaN,NaN,NaN,2,NaN,10,0,NaN,1,2,NaN,NaN,NaN,0,10,8,3,3,3,10,0,4,0,10}
181	Make up a song	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,6,3,0,8,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,NaN,0,2,8,NaN,NaN,NaN,NaN,1,9,4,4,10,0,2,0,10}
182	Read a book about pioneers	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,5,2,4,NaN,NaN,NaN,NaN,NaN,10,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,0,10,9,1,NaN,NaN,10,0,4,0,10}
183	Sew a new costume	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,5,1,6,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,3,3,8,NaN,NaN,0,10,6,6,NaN,NaN,10,0,8,0,10}
184	Have a Nerf Gun battle	https://www.google.com/search?q=Nerf+gun+battle	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{10,0,8,8,3,0,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,4,6,7,3,NaN,10,2,0,10}
185	Perform a puppet show	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,8,4,0,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,3,NaN,NaN,NaN,0,10,6,6,5,NaN,NaN,10,2,0,10}
186	Volunteer at the Humane Society	https://maps.google.com/maps?sll=%F&q=Humane+Society	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{3,7,0,0,8,8,NaN,NaN,10,10,0,NaN,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,3,7,NaN,8,8,8,4,8,4,0,10}
187	Play Frisbee	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{4,6,0,8,5,1,NaN,NaN,NaN,10,NaN,NaN,NaN,NaN,NaN,NaN,0,3,NaN,NaN,NaN,10,0,9,NaN,5,NaN,NaN,4,1,0,10}
188	Play pool	https://maps.google.com/maps?sll=%F&q=pool+hall	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{1,9,0,1,5,3,NaN,NaN,NaN,3,8,NaN,NaN,NaN,8,NaN,2,7,NaN,NaN,4,0,10,3,8,5,5,3,8,2,0,10}
189	Do exercises	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{10,0,0,0,5,7,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,NaN,NaN,NaN,7,NaN,NaN,4,2,NaN,1,0,10}
190	Visit the Hubble Space Telescope picture gallery	http://www.hubblesite.org/gallery	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,5,8,NaN,5,NaN,NaN,NaN,8,NaN,3,6,NaN,0,5,NaN,NaN,NaN,0,10,8,4,0,10,7,3,1,10,0}
191	Play in a band	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,2,5,1,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,8,NaN,NaN,NaN,0,10,6,6,3,3,NaN,10,4,0,10}
192	Go to a pet store to look at kittens	https://maps.google.com/maps?sll=%F&q=pet+store	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,7,4,3,NaN,NaN,NaN,NaN,NaN,NaN,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,10,1,3,4,NaN,2,5,1,0,10}
193	Teach yourself how to juggle	https://www.google.com/search?q=teach+yourself+how+to+juggle	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{1,9,0,2,4,0,NaN,NaN,NaN,NaN,NaN,3,NaN,NaN,NaN,NaN,0,8,NaN,NaN,NaN,5,5,8,NaN,2,4,8,0,3,0,10}
194	Take a humanities class	https://www.google.com/search?q=humanities+courses	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,4,2,0,10,0,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,3,5,NaN,NaN,NaN,0,10,9,1,3,3,1,9,10,10,0}
195	Swap music with a friend	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,3,8,8,3,NaN,NaN,NaN,NaN,NaN,8,8,3,NaN,0,5,NaN,NaN,NaN,0,10,NaN,8,7,3,0,10,1,10,0}
196	Go window shopping downtown	\N	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{2,8,0,0,6,8,NaN,NaN,NaN,NaN,NaN,1,NaN,9,0,NaN,1,2,NaN,NaN,7,8,2,3,NaN,NaN,NaN,NaN,5,2,0,10}
197	Make meals for charity	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,4,2,NaN,NaN,10,5,NaN,NaN,NaN,7,NaN,9,4,3,4,NaN,NaN,0,10,NaN,NaN,NaN,5,4,NaN,4,0,10}
198	Sample songs in iTunes	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,2,8,4,1,NaN,NaN,NaN,NaN,NaN,3,NaN,NaN,NaN,0,2,NaN,NaN,NaN,0,10,8,3,NaN,NaN,10,0,1,10,0}
199	Play with household pets	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,8,6,7,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,4,NaN,5,5,3,8,7,NaN,7,3,1,10,0}
200	Read remodeling magazines	https://www.google.com/search?q=remodeling+magazines	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,2,8,2,NaN,NaN,NaN,NaN,2,NaN,7,5,NaN,1,4,NaN,NaN,NaN,0,10,3,8,NaN,NaN,10,0,2,10,0}
201	Play hopscotch	http://en.wikipedia.org/wiki/Hopscotch	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{7,3,0,10,0,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,10,0,9,1,5,NaN,NaN,10,1,0,10}
202	Do calligraphy	https://www.google.com/search?q=learn+calligraphy	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{1,9,0,0,5,7,6,NaN,NaN,NaN,NaN,3,NaN,NaN,NaN,NaN,1,7,7,NaN,NaN,0,10,NaN,NaN,NaN,NaN,8,3,4,0,10}
203	Ride a horse	https://maps.google.com/maps?sll=%F&q=horseback+riding	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{5,5,0,2,3,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,8,4,NaN,9,NaN,10,0,NaN,6,3,NaN,NaN,NaN,6,0,10}
204	Invent a new cookie recipe	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,4,8,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,8,NaN,10,NaN,2,7,NaN,NaN,0,10,NaN,6,4,NaN,NaN,NaN,3,0,10}
205	Write a story	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,4,1,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,10,6,NaN,NaN,0,10,NaN,8,NaN,NaN,10,0,8,0,10}
206	Go to an amusement park	https://maps.google.com/maps?sll=%F&q=amusement+park	n/a or 	\N	0	10	15	NaN	SR+4	SS-1	{4,6,0,7,6,3,NaN,NaN,NaN,8,3,NaN,NaN,NaN,NaN,NaN,4,4,NaN,NaN,4,8,2,8,3,7,NaN,6,6,4,0,10}
207	Read parts from a script and each play a character	http://www.simplyscripts.com/plays.html	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{2,8,0,1,3,1,3,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,6,6,8,8,8,3,3,0,10}
208	Play poker online	https://www.google.com/search?q=play+poker+online	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,3,4,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,7,NaN,0,6,NaN,NaN,NaN,0,10,4,NaN,NaN,3,9,NaN,2,10,0}
209	Play miniature golf	https://maps.google.com/maps?sll=%F&q=miniature+golf	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{3,7,0,10,8,3,NaN,NaN,NaN,NaN,NaN,NaN,3,5,5,NaN,2,5,NaN,NaN,NaN,10,0,8,2,10,NaN,0,10,3,0,10}
210	Listen to random podcasts	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,3,NaN,NaN,NaN,NaN,NaN,5,3,NaN,NaN,NaN,NaN,6,NaN,NaN,NaN,0,10,5,5,NaN,NaN,10,0,1,10,0}
211	Watch the sunset	\N	n/a or 	not OVC	0	10	15	NaN	SS-1	SS	{2,8,0,2,3,3,NaN,NaN,NaN,3,8,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,5,NaN,10,0,8,3,NaN,NaN,6,6,1,0,10}
212	Go for a walk	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{5,5,0,1,5,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,2,NaN,NaN,NaN,10,0,NaN,2,NaN,NaN,NaN,5,1,0,10}
213	Start a compost pile 	https://www.google.com/search?q=how+to+start+a+compost+pile+	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{3,7,0,1,4,1,NaN,NaN,NaN,7,NaN,NaN,NaN,4,NaN,NaN,3,2,NaN,NaN,NaN,10,0,9,1,NaN,NaN,9,1,4,10,0}
214	Go without electricity for one evening	\N	\N	\N	NaN	NaN	NaN	NaN	SS	SR-1	{2,8,0,0,7,4,NaN,NaN,NaN,0,10,2,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,3,8,5,5,NaN,NaN,4,0,10}
215	Look at travel pictures	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,4,6,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,10,NaN,5,NaN,NaN,NaN,1,5,5}
216	Tie dye a T-shirt	https://www.google.com/search?q=how+to+tie+dye+a+T-shirt	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{3,7,0,0,5,5,8,NaN,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,2,4,7,NaN,NaN,5,5,5,3,4,NaN,5,5,3,0,10}
217	Go for a drive	\N	n/a or 	\N	0	10	NaN	NaN	SR+3	SS	{0,10,0,0,4,8,NaN,NaN,NaN,6,NaN,1,NaN,NaN,7,NaN,1,3,NaN,NaN,NaN,9,1,NaN,7,NaN,NaN,NaN,NaN,2,0,10}
218	Use Google Earth to explore remote islands	http://earth.google.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,3,5,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,10,0,1,10,0}
219	Play gin rummy with friends	https://www.google.com/search?q=how+to+play+gin+rummy	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,2,5,3,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,NaN,0,6,NaN,NaN,NaN,0,10,4,NaN,3,NaN,NaN,7,2,0,10}
220	Give yourself a haircut	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{1,9,0,0,5,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,NaN,2,NaN,NaN,NaN,0,10,9,1,NaN,NaN,10,0,1,0,10}
221	Go play at Free Rice (it's for a good cause)	http://www.freerice.com	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,7,3,NaN,NaN,1,NaN,NaN,2,NaN,NaN,NaN,NaN,NaN,5,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,1,10,0}
222	Go hunting	https://maps.google.com/maps?sll=%F&q=hunting	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{8,2,10,0,0,10,NaN,NaN,NaN,10,0,NaN,NaN,NaN,8,NaN,6,7,NaN,10,NaN,10,0,6,6,NaN,7,8,3,8,0,10}
223	Bake cake or cupcakes	https://www.google.com/search?q=cupcake+recipes	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{4,6,0,1,7,7,NaN,NaN,NaN,NaN,NaN,NaN,NaN,7,3,7,4,2,4,NaN,NaN,0,10,8,NaN,3,NaN,NaN,NaN,2,0,10}
224	Watch TV	http://tvguide.com	\N	\N	NaN	NaN	NaN	NaN	SR+1	SS+6	{0,10,0,8,6,9,NaN,NaN,NaN,NaN,NaN,1,6,NaN,NaN,NaN,0,3,NaN,NaN,NaN,0,10,9,NaN,3,3,NaN,3,3,10,0}
225	Play Go with friends	https://www.google.com/search?q=how+to+play+Go	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{0,10,0,1,4,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,8,NaN,NaN,NaN,0,10,4,NaN,NaN,3,NaN,7,2,0,10}
226	Teach yourself hacky sack	https://www.google.com/search?q=teach+yourself+hacky+sack	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{1,9,0,2,3,0,NaN,NaN,NaN,5,NaN,3,NaN,NaN,NaN,NaN,0,8,NaN,NaN,NaN,5,5,8,NaN,2,4,8,0,3,0,10}
227	Go to the library and read random magazines and books	https://maps.google.com/maps?sll=%F&q=library	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{2,8,0,0,4,7,NaN,NaN,NaN,8,3,8,NaN,NaN,NaN,NaN,NaN,4,NaN,NaN,NaN,0,10,1,9,3,3,8,3,2,0,10}
229	Go whitewater rafting 	https://maps.google.com/maps?sll=%F&q=whitewater+rafting	n/a or 	\N	0	10	21	NaN	SR+3	SS-1	{8,2,0,2,8,4,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,4,5,NaN,10,NaN,10,0,3,8,NaN,NaN,NaN,10,4,0,10}
230	Write a letter to an old friend	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,3,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,5,2,NaN,NaN,0,10,6,6,NaN,NaN,5,5,1,5,5}
231	Write in a journal	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,4,2,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,3,8,NaN,NaN,10,0,1,0,10}
232	Host a clothing exchange with friends	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{3,7,0,0,4,6,NaN,NaN,NaN,NaN,NaN,NaN,NaN,10,0,NaN,0,NaN,NaN,NaN,NaN,0,10,9,1,7,NaN,NaN,10,3,0,10}
234	Look for geocaches	http://www.geocaching.com/	n/a or 	\N	0	10	10	NaN	SR+3	SS-1	{5,5,0,5,5,0,NaN,NaN,NaN,10,NaN,1,NaN,NaN,NaN,NaN,0,3,NaN,NaN,NaN,10,0,6,NaN,NaN,NaN,NaN,NaN,6,0,10}
235	Make a wood fire	\N	\N	\N	NaN	NaN	NaN	NaN	SS	SS+4	{4,6,0,0,5,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,0,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,8,0,1,0,10}
236	Volunteer at a senior center	https://maps.google.com/maps?sll=%F&q=senior+center	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{3,7,0,0,5,6,NaN,NaN,7,8,3,2,NaN,NaN,NaN,NaN,0,3,NaN,NaN,NaN,0,10,8,3,4,2,NaN,10,4,0,10}
237	Organize a neighborhood potluck	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{3,7,0,0,5,4,NaN,NaN,NaN,8,NaN,NaN,NaN,8,3,10,2,3,8,NaN,NaN,0,10,7,5,4,NaN,NaN,10,4,0,10}
238	Play soccer	https://maps.google.com/maps?sll=%F&q=soccer+field	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{10,0,0,8,8,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,0,10,NaN,NaN,NaN,9,1,4,NaN,3,NaN,NaN,10,2,0,10}
239	Read new recipes online	https://www.google.com/search?q=recipes	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,0,8,2,NaN,NaN,NaN,NaN,NaN,3,NaN,8,2,9,0,3,NaN,NaN,NaN,0,10,NaN,7,NaN,NaN,7,NaN,3,10,0}
240	Climb a tree	\N	n/a or 	\N	0	10	10	NaN	SR+3	SS	{6,4,0,10,3,0,NaN,NaN,NaN,10,0,NaN,NaN,NaN,NaN,NaN,NaN,5,NaN,8,NaN,10,0,NaN,6,3,NaN,8,3,1,0,10}
241	Play Wii	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,8,8,0,NaN,NaN,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,1,8,NaN,NaN,NaN,0,10,7,NaN,6,NaN,NaN,4,1,10,0}
242	Go out for ice cream	https://maps.google.com/maps?sll=%F&q=ice+cream	\N	\N	NaN	NaN	NaN	NaN	SR+4	SS+4	{3,7,0,5,6,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,5,2,1,NaN,NaN,2,10,0,6,NaN,5,NaN,NaN,4,1,0,10}
243	Read a fiction book	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,6,8,8,5,NaN,NaN,NaN,NaN,3,6,NaN,NaN,NaN,1,7,NaN,NaN,NaN,0,10,4,NaN,NaN,NaN,10,0,3,0,10}
244	Rearrange your living room	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+3	{6,4,0,0,2,8,3,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,0,2,NaN,NaN,NaN,0,10,6,6,NaN,NaN,10,0,4,0,10}
245	Learn self-defense	https://www.google.com/search?q=Learn+self-defense	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{10,0,10,2,8,4,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,10,8,2,NaN,10,2,8,6,0,10}
246	Knit a blanket for charity	\N	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{2,8,0,0,2,8,4,NaN,10,NaN,NaN,NaN,NaN,9,0,NaN,1,2,8,NaN,NaN,0,10,3,8,NaN,NaN,7,3,6,10,0}
247	Play checkers online	https://www.google.com/search?q=play+checkers+online	\N	\N	NaN	NaN	NaN	NaN	\N	\N	{0,10,0,1,4,3,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,4,NaN,NaN,NaN,0,10,4,NaN,3,NaN,9,NaN,1,10,0}
175	Perform a musical instrument for friends	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{2,8,0,6,3,3,10,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,0,NaN,NaN,NaN,NaN,0,10,8,3,3,3,NaN,10,3,0,10}
228	Go dancing	\N	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS+4	{7,3,0,1,5,8,NaN,NaN,NaN,NaN,8,NaN,NaN,NaN,NaN,NaN,2,6,NaN,NaN,NaN,0,10,3,8,6,NaN,NaN,10,3,0,10}
166	Go to a lake and feed the ducks	\N	n/a or 	\N	0	10	15	NaN	SR+3	SS-1	{2,8,0,7,5,3,NaN,NaN,5,10,0,NaN,NaN,NaN,NaN,NaN,NaN,3,NaN,10,NaN,10,0,NaN,6,NaN,3,NaN,NaN,2,0,10}
233	Take a family portrait	https://maps.google.com/maps?sll=%F&q=portrait+studio	\N	\N	NaN	NaN	NaN	NaN	SR+3	SS-1	{0,10,0,0,8,8,NaN,NaN,NaN,NaN,NaN,NaN,NaN,7,3,NaN,NaN,NaN,NaN,NaN,NaN,0,10,10,NaN,NaN,NaN,8,3,1,0,10}
\.


--
-- Name: activities_activity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('activities_activity_id_seq', 247, true);


--
-- PostgreSQL database dump complete
--

