# Fun Genie

![Codeship Status](https://codeship.com/projects/23c96870-7b96-0133-766d-7a77bd807782/status?branch=develop)

App to suggest activities according to location, weather conditions and temperature.
Django app based on Fun Genie [iOS app](https://itunes.apple.com/mx/app/fun-genie/id593811673?l=en&mt=8).

# Tech
- Python
- Django
- PostgreSQL
- Heroku
- Codeship

# Setup
Create a clean environment to run the project
```
virtualenv myvenv
source myvenv/bin/activate
```
Install the dependencies

`$ pip install -r requirements.txt`

# Running locally

## Database

Set a `DATABASE_URL` environment variable to a valid PostgreSQL URI:

`$ export DATABASE_URL="postgres://<user>:<password>@localhost:5432/<database_name>"`

Substitute `<user>`, `<password>` and `<database_name>` to match your
local configuration.

Run the migrations afterwards:

`$ ./manage.py migrate`

Additionally, an `activities.sql` SQL dump file is available with test data.

## Web Server

You can use Django's development server:

`$ ./manage.py runserver`

If you'd like to run a production-like server (recommended):

1. Deploy static files:

    `$ ./manage.py collectstatic --no-input`

2. Run [Gunicorn](http://gunicorn.org/) from the same location `manage.py` is:

    `$ gunicorn fun_genie.wsgi`

# Deployments
The deployment proccess to heroku is being handle by Codeship automatically.

| Branch | URL |
| ------ | ------ |
| Develop | https://fungenie-staging.herokuapp.com |
| Master | N/A |
