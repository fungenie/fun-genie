$(document).ready(function (){
    var timezone = jstz.determine();
    document.cookie = 'timezone="' + timezone.name() + '"';
    window.location = '/activities';
});