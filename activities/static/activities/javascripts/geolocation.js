$(document).ready(function (){

    $(".btn").attr("disabled", true);
    tryAPIGeolocation();

    function tryAPIGeolocation(){
	    jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyB7Yf4XjxWSpQLBLYxI4Ty1afgPhYF8AtQ",
            function(success) {
		        success_handler({coords: {latitude: success.location.lat, longitude: success.location.lng}});
        })
        .fail(function(err) {
            error_handler()
        });
    };

    function success_handler(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        accuracy = position.coords.accuracy;

        document.cookie = 'posLat="' + latitude + '"';
        document.cookie = 'posLong="' + longitude + '"';
        $(".btn").attr("disabled", false);
    }

    function  error_handler(){
        document.innerHTML = "Geolocation is not supported by this browser.";
    }
});