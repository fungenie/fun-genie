$(document).ready(function (){
    $('#on-button').click(function (){
        document.cookie = 'use_time_and_weather=True'
        $('#on-button').removeClass('btn-default').addClass('btn-success')
        $('#off-button').removeClass('btn-danger').addClass('btn-default')
    })

    $('#off-button').click(function (){
        document.cookie = 'use_time_and_weather=False'
        $('#off-button').removeClass('btn-default').addClass('btn-danger')
        $('#on-button').removeClass('btn-success').addClass('btn-default')
    })
})