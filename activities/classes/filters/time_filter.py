from datetime import datetime, timedelta

SUNRISE = 'sunrise'
SUNSET = 'sunset'
TIME = 'time'

SUNRISE_CODE = 'SR'
SUNSET_CODE = 'SS'

START_TIME = 'start_time'
END_TIME = 'end_time'

SETTING = 'setting'
OPERATION = 'operation'
HOURS = 'hours'

def is_not_within_time_bounds(current_time, time_bounds):
    return not (time_bounds[0] <= current_time < time_bounds[1])

def apply_operation(time, operation, hour_count):
    if operation is not None:
        if operation == '+':
            return time + timedelta(hours = hour_count)
        elif operation == '-':
            return time - timedelta(hours = hour_count)
    else:
        return time

def parse_offset(offset):
    parsed_offset = dict()
    parsed_offset[SETTING] = offset[:2]
    try:
        parsed_offset[OPERATION] = offset[2]
        parsed_offset[HOURS] = int(offset[3:])
    except IndexError:
        parsed_offset[OPERATION] = None
        parsed_offset[HOURS] = None
    return parsed_offset

def get_time(parsed_offset, current_sunrise, current_sunset):
    if parsed_offset[SETTING] == SUNRISE_CODE:
        time = current_sunrise
    elif parsed_offset[SETTING] == SUNSET_CODE:
        time = current_sunset
    return apply_operation(time, parsed_offset[OPERATION], parsed_offset[HOURS])

def has_time_constraints(activity_info):
    return all((activity_info[START_TIME], activity_info[END_TIME]))

def parse_time(time):
    return datetime.strptime(time, '%Y-%m-%d %H:%M')

def filter_activities(activities, timeJSON):

    current_sunrise = parse_time(timeJSON[SUNRISE])
    current_sunset = parse_time(timeJSON[SUNSET])
    current_time = parse_time(timeJSON[TIME])

    for activity_title, activity_info in activities.items():
        if has_time_constraints(activity_info):

            activity_offset_bounds = [
                    activity_info[START_TIME],
                    activity_info[END_TIME]]

            activity_time_bounds = [
                    get_time(parse_offset(i), current_sunrise, current_sunset)
                    for i in activity_offset_bounds]

            if is_not_within_time_bounds(current_time, activity_time_bounds):
                del activities[activity_title]

    return activities
