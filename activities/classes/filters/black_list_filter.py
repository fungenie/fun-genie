class BlackListFilter:
    def __init__(self, black_list, activty_list):
        self.black_list = black_list
        self.activity_list = activty_list

    def filter_by_black_list(self):
        filtered_activities = self.activity_list

        if len(self.black_list) > 0:
            for activity in self.black_list:
                if activity in self.activity_list:
                    if type(filtered_activities) == list:
                        filtered_activities.remove(activity)
                    if type(filtered_activities) == dict:
                        del filtered_activities[activity]
            return filtered_activities
        else:
            return self.activity_list
