from black_list_filter import BlackListFilter
from time_filter import filter_activities as filter_by_time
from weather_filter import WeatherFilter
import copy

class FilterManager:
    def __init__(self):
        pass

    def apply_filters(self, activity_list, black_list, timeJSON=None, weatherJSON=None):
        black_list_filter = BlackListFilter(black_list, activity_list)
        filtered_activity_list = black_list_filter.filter_by_black_list()

        if timeJSON and weatherJSON:
            filtered_activity_list = filter_by_time(filtered_activity_list, timeJSON)
            weather_filter = WeatherFilter(filtered_activity_list, weatherJSON)
            filtered_activity_list = weather_filter.filter_activities_by_weather()
        filtered_hash = copy.copy(filtered_activity_list)
        filtered_activity_list = []
        for key in filtered_hash:
            filtered_activity_list.append(key)
        return filtered_activity_list
