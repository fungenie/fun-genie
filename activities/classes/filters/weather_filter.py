import math

class Weather:
    def __init__(self, conditions, cloud_code, temperature, wind_speed):
        self.conditions = conditions
        self.cloud_code = cloud_code
        self.temperature = temperature
        self.wind_speed = wind_speed


class WeatherFilter:
    def __init__(self, activity_hash, weather_json):
        self.activity_hash = activity_hash
        self.weather = weather_json

    def filter_activity_by_weather(self, activity_name):

        if self.activity_hash[activity_name]['unacceptable_clouds_code']:
            if self.activity_hash[activity_name]['unacceptable_clouds_code'] == self.weather['cloud_code']:
                return False

        if self.activity_hash[activity_name]['acceptable_weather_conditions'] is not None:
            if self.activity_hash[activity_name]['acceptable_weather_conditions'] != self.weather['weatherCondition']:
                return False

        if not math.isnan(self.activity_hash[activity_name]['min_temp']):
            if self.weather['temperature'] < self.activity_hash[activity_name]['min_temp']:
                return False

        if not math.isnan(self.activity_hash[activity_name]['max_temp']):
            if self.weather['temperature'] > self.activity_hash[activity_name]['max_temp']:
                return False

        if not math.isnan(self.activity_hash[activity_name]['min_wind_speed']):
            if self.weather['wind_speed'] < self.activity_hash[activity_name]['min_wind_speed']:
                return False

        if not math.isnan(self.activity_hash[activity_name]['min_wind_speed']):
            if self.weather['wind_speed'] > self.activity_hash[activity_name]['max_wind_speed']:
                return False
        return True

    def filter_activities_by_weather(self):
        filtered_list = {}
        for activity in self.activity_hash:
            if self.filter_activity_by_weather(activity):
                filtered_list[activity] = self.activity_hash[activity]
        return filtered_list
