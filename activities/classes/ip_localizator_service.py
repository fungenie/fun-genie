import urllib
import json

class Ip_localizator_service:
  def get_api_response(self, ip):
    response = urllib.urlopen('http://ip-json.rhcloud.com/json/' + ip).read()
    return response

  def get_localization(self, localization_string):
    return json.loads(localization_string)
