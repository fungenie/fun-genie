import math
import json
import copy
import random
import os
import logging
from filters.filter_manager import FilterManager
from filters.black_list_filter import BlackListFilter
from django.conf import settings
from activities.models import Activity
from activities.exceptions import NoPossibleActivitiesError

UP_VOTE = 1
DOWN_VOTE = -1
NEVER_VOTE = -2

HIGH_BAND_GAIN = 0.82
VOTING_GAIN = 4

class ActivityEngineInfo:
    def __init__(self, activity):
        self.activity = activity
        self.has_user_vote = False
        self.vote = None
        # convert NaNs in activity's weights to 0s
        # Shouldn't weights of NaN be 0s already?
        for i in range(0, len(self.activity['weights'])):
            if math.isnan(self.activity['weights'][i]):
                self.activity['weights'][i] = 0
        self.activity['original_weights'] = copy.copy(self.activity['weights'])

class ActivitySuggestionEngine:
    vote_count = 0

    def __init__(self, votes={}, blacklist=[], timeJSON=None, weatherJSON=None):

        # To maintain compatibility with the current algorithms, all
        # activities are converted to a dictionary of title: parameters.

        activities = dict()
        activities_as_list = list(Activity.objects.all())
        for activity in activities_as_list:
            activities[activity.name] = activity.__dict__

        activity_infos = {}
        filter_manager = FilterManager()
        filtered_activities = filter_manager.apply_filters(activities, blacklist, timeJSON, weatherJSON)

        self.blacklist = blacklist
        self.filtered_activities = filtered_activities
        self.voted_activities = []

        for activity_name in activities:
            activity = activities[activity_name]
            info = ActivityEngineInfo(activity)
            activity_infos[activity_name] = info
        self.activity_infos = activity_infos

        attribute_deltas = []
        for key in self.filtered_activities:
            for weight in self.activity_infos[key].activity['weights']:
                attribute_deltas.append(0.0)
            break
        self.attribute_deltas = attribute_deltas

        # Compute votes
        for activity, vote in votes.items():
            self.made_vote(vote, activity)

    def reset(self):
        # When should this method be called, if activities are now stored in a database?
        # Shouldn't it be a stored procedure in the database server?
        self.filtered_activities = self.filtered_activities + copy.copy(self.voted_activities)
        self.filtered_activities.sort()
        self.voted_activities = []
        for key in self.filtered_activities:
            self.activity_infos[key].has_user_vote = False
            self.activity_infos[key].vote = None
            self.activity_infos[key].activity['weights'] = copy.copy(
                self.activity_infos[key].activity['original_weights'])
        self.vote_count = 0
        for i in range(0, len(self.attribute_deltas)):
            self.attribute_deltas[i] = 0.0

    def made_vote(self, vote, activity_name):
        try:
            self.vote_count += 1
            self.voted_activities.append(activity_name)

            voted_info = copy.copy(self.activity_infos[activity_name]) # raises KeyError
            voted_info.vote = vote

            # Never votes are taken as negative
            # why not?
            vote_in_blacklist = False
            if vote == NEVER_VOTE:
                vote = DOWN_VOTE
                vote_in_blacklist = True

            # Update attribute deltas
            for i in range(0, len(voted_info.activity['weights'])):
                voted_info.activity['weights'][i] *= vote
                sum = 0
                for key in self.voted_activities:
                    sum += self.activity_infos[key].activity['weights'][i]
                avg = sum / self.vote_count
                self.attribute_deltas[i] = VOTING_GAIN * avg
            self.activity_infos[activity_name] = voted_info

            # Update weights of all the sessions activities based on the new deltas
            for key in self.filtered_activities:
                info = copy.copy(self.activity_infos[key])
                for i in range(0, len(info.activity['weights'])):
                    info.activity['weights'][i] = info.activity['original_weights'][i] + self.attribute_deltas[i]
                self.activity_infos[key] = info
            if not vote_in_blacklist and activity_name in self.filtered_activities:
                self.filtered_activities.remove(activity_name)
        except KeyError as e:
            # activity is blacklisted, therefore not available in activity_infos
            logger = logging.getLogger(__name__)
            logger.info(u'{} is blacklisted, skipping...'.format(activity_name))

    def compute_suggestion(self, votes):
        duplicate_filter = BlackListFilter(votes, self.filtered_activities)
        activities_without_votes = duplicate_filter.filter_by_black_list()

        # Find the globally maximum attribute value
        max_attribute_value = 0
        for key in activities_without_votes:
            info = copy.copy(self.activity_infos[key])
            for weight in info.activity['weights']:
                max_attribute_value = max(max_attribute_value, weight)

        # Compute the high band base (min value for an attribute to be 'dominant')
        high_band_base = max_attribute_value * HIGH_BAND_GAIN

        # Count dominant attributes per activity
        count_high_band = {}
        for key in self.filtered_activities:
            info = copy.copy(self.activity_infos[key])
            count_high_band[key] = 0
            for weight in info.activity['weights']:
                if weight >= high_band_base:
                    count_high_band[key] += 1

        # Find optimal activity to propose (first one with the fewest > 0 dominant attributes)
        # 1) Find the globally lowest number of attributes in the high band
        min_count_high_band = 999
        for key in self.filtered_activities:
            if count_high_band[key] < min_count_high_band and count_high_band[key] > 0:
                min_count_high_band = count_high_band[key]

        # 2) Get a random activity number of dominant attributes
        possible_activities = []
        for key in count_high_band:
            if count_high_band[key] == min_count_high_band:
                possible_activities.append(key)

        # If there aren't any activities at all, ``possible_activities'' won't even be full, raising a ValueError.
        # There is a change the algorithm might run out of ideas, causing ``possible_activities'' to be empty too.
        try:
            activity = possible_activities[random.randint(0, len(possible_activities) - 1)] # raises ValueError
        except ValueError as ex:
            raise NoPossibleActivitiesError('possible_activities is empty!');

        while activity in self.blacklist or activity in votes.keys():
            activity = possible_activities[random.randint(0, len(possible_activities) - 1)]
            if (activity in votes.keys() or activity in self.blacklist) and len(possible_activities) == 1:
                activity = 'Sorry, Genie ran out of ideas!'
                break

        result = {'name': activity}
        if activity is not 'Sorry, Genie ran out of ideas!':
            if self.activity_infos[activity].activity['url']:
                result['url'] = self.activity_infos[activity].activity['url']
            else:
                result['url'] = None
        else:
            result['url'] = None

        # for logging purposes
        result['start_time'] = self.activity_infos[activity].activity['start_time']
        result['end_time'] = self.activity_infos[activity].activity['end_time']

        return result
