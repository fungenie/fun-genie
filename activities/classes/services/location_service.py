import codecs
import logging
import urllib2
import json


class LocationService:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def get_time_data_from_api(self):
        try:
            url = "http://api.geonames.org/timezoneJSON?lat=" + self.latitude + "&lng=" + self.longitude + "&username=fungenie"
            response = urllib2.urlopen(url)
            json_data = json.loads(response.read());
        except urllib2.HTTPError, e:
            logging.exception("Exception " + e)
            raise
        except urllib2.URLError, e:
            logging.exception("Exception " + e)
            raise
        except(json.decoder.JSONDecodeError, ValueError):
            logging.info('Time Decode JSON has failed')
            raise
        return json_data

    def get_weather_info(self):
        try:
            reader = codecs.getreader('utf-8')
            weather_service_url = 'http://ws.geonames.net/findNearByWeatherJSON?lat=' + self.latitude + '&lng=' + self.longitude + '&username=bashmore'
            json_data = json.load(reader(urllib2.urlopen(weather_service_url)))
        except urllib2.HTTPError, e:
            logging.exception("Exception: " + e)
            raise
        except urllib2.URLError, e:
            logging.exception("Exception: " + e)
            raise
        except(json.decoder.JSONDecodeError, ValueError):
            logging.info('Weather Decode JSON has failed ' + ValueError)
            raise
        return json_data
