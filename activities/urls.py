from django.conf.urls import url

from .views import contact, neverlist, views

urlpatterns = [
  url(r'^$', views.home),

  # There should be a better naming scheme for the same view
  # ``activities'', don't you think?

  url(r'^activities/$', views.activities, name='activities_root'),
  url(r'^activities/(.*)$', views.activities, name='activities'),
  url(r'^vote/(.*)/(-?\d)', views.vote, name='vote'),
  url(r'^reset', views.reset_votes, name='reset'),
  url(r'^preferences', views.preferences, name='preferences'),
  url(r'^neverlist/$', neverlist.index, name='neverlist'),
  url(r'^neverlist/delete/$', neverlist.delete, name='neverlist_delete'),
  url(r'^contact', contact.index, name='contact'),
  url(r'^thankyou', contact.thankyou, name='contact_thankyou'),
  url(r'^no_activities', views.noactivities),
  url(r'^bye', views.bye_session, name='bye'),
  url(r'^reset_votes', views.reset_votes, name='reset_votes'),
  url(r'^init_timezone', views.init_timezone, name='init_timezone')
]
