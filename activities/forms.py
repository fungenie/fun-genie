from django import forms


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'subject_text_field'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'message_text_field'}))
