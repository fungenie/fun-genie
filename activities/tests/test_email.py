from django.test import TestCase
from django.core import mail


class EmailTest(TestCase):
    def test_send_email(self):

        mail.outbox = []

        mail.send_mail(
            'Subject here', 'Here is the message.',
            'from@example.com', ['to@example.com'],
        )

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Subject here')
        self.assertEqual(mail.outbox[0].body, 'Here is the message.')
        self.assertEqual(mail.outbox[0].from_email, 'from@example.com')
        self.assertEqual(len(mail.outbox[0].to), 1)
        self.assertItemsEqual(mail.outbox[0].to, ['to@example.com'])
