from datetime import datetime

from django.test import TestCase

from activities.classes.filters.time_filter import *


class FilterTest(TestCase):

    def test_apply_add_operation(self):
        time = datetime(2017, 5, 12, 10)
        operation = '+'
        hours = 5

        expected_time = datetime(2017, 5, 12, 15)

        self.assertEqual(apply_operation(time, operation, hours),
                expected_time)

    def test_apply_substract_operation(self):
        time = datetime(2017, 5, 12, 10)
        operation = '-'
        hours = 5

        expected_time = datetime(2017, 5, 12, 5)

        self.assertEqual(apply_operation(time, operation, hours),
                expected_time)

    def test_apply_none_operation(self):
        time = datetime(2017, 5, 12, 10)
        operation = None
        hours = None

        expected_time = datetime(2017, 5, 12, 10)

        self.assertEqual(apply_operation(time, operation, hours),
                expected_time)

    def test_parse_plain_offset(self):
        offset = 'SR'

        expected_setting = 'SR'
        expected_operation = None
        expected_hours = None

        obtained_offset = parse_offset(offset)

        self.assertEqual(obtained_offset['setting'], expected_setting)
        self.assertEqual(obtained_offset['operation'], expected_operation)
        self.assertEqual(obtained_offset['hours'], expected_hours)

    def test_parse_adding_offset(self):
        offset = 'SS+5'

        expected_setting = 'SS'
        expected_operation = '+'
        expected_hours = 5

        obtained_offset = parse_offset(offset)

        self.assertEqual(obtained_offset['setting'], expected_setting)
        self.assertEqual(obtained_offset['operation'], expected_operation)
        self.assertEqual(obtained_offset['hours'], expected_hours)

    def test_get_time_plain_sunrise(self):
        offset = {
                'setting': 'SR',
                'operation': None,
                'hours': None}

        sunrise = datetime(2017, 5, 12, 5)

        expected_time = datetime(2017, 5, 12, 5)

        self.assertEqual(get_time(offset, sunrise, None), expected_time)

    def test_get_time_adding_sunrise(self):
        offset = {
                'setting': 'SR',
                'operation': '+',
                'hours': 5}

        sunrise = datetime(2017, 5, 12, 5)

        expected_time = datetime(2017, 5, 12, 10)

        self.assertEqual(get_time(offset, sunrise, None), expected_time)

    def test_get_time_substracting_sunrise(self):
        offset = {
                'setting': 'SR',
                'operation': '-',
                'hours': 5}

        sunrise = datetime(2017, 5, 12, 5)

        expected_time = datetime(2017, 5, 12, 0)

        self.assertEqual(get_time(offset, sunrise, None), expected_time)

    def test_is_within_range(self):
        current_time = datetime(2017, 5, 12, 11)
        time_bounds = [datetime(2017, 5, 12, 10), datetime(2017, 5, 12, 12)]

        self.assertFalse(is_not_within_time_bounds(current_time, time_bounds))

    def test_is_not_within_range(self):
        current_time = datetime(2017, 5, 12, 9)
        time_bounds = [datetime(2017, 5, 12, 10), datetime(2017, 5, 12, 11)]

        self.assertTrue(is_not_within_time_bounds(current_time, time_bounds))

    def test_filter_activities(self):

        timeJSON = {'sunrise': '2017-05-13 5:00',
                    'sunset': '2017-05-13 18:00',
                    'time': '2017-05-13 10:00'}

        unfiltered_activities = {
                'activity_1': {'start_time': 'SS+1', 'end_time': 'SS+3'},
                'activity_2': {'start_time': 'SR-2', 'end_time': 'SR'},
                'activity_3': {'start_time': 'SS', 'end_time': 'SS+4'},
                'activity_4': {'start_time': 'SR+5', 'end_time': 'SS'},
                'activity_5': {'start_time': 'SR+3', 'end_time': 'SS-1'}}

        expected_activities = {
                'activity_4': {'start_time': 'SR+5', 'end_time': 'SS'},
                'activity_5': {'start_time': 'SR+3', 'end_time': 'SS-1'}}

        self.assertDictEqual(filter_activities(unfiltered_activities, timeJSON), expected_activities)

    def test_filter_activities_with_no_time_constraints(self):

        timeJSON = {'sunrise': '2017-05-13 5:00',
                    'sunset': '2017-05-13 18:00',
                    'time': '2017-05-13 10:00'}

        unfiltered_activities = {
                'activity_1': {'start_time': None, 'end_time': None},
                'activity_2': {'start_time': 'SR+3', 'end_time': 'SS-1'}}

        expected_activities = {
                'activity_1': {'start_time': None, 'end_time': None},
                'activity_2': {'start_time': 'SR+3', 'end_time': 'SS-1'}}

        self.assertDictEqual(filter_activities(unfiltered_activities, timeJSON), expected_activities)
