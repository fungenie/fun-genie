from django.test import TestCase
from activities.models import Activity
from activities.classes.filters.black_list_filter import BlackListFilter


class BlacklistTest(TestCase):
    def test_filter(self):
        # Blacklisting is done comparing strings. Wouldn't it be better if it
        # was done by IDs?

        activities = dict()

        activity_1 = Activity()
        activity_1.name = 'activity_1'

        activity_2 = Activity()
        activity_2.name = 'activity_2'

        activity_3 = Activity()
        activity_3.name = 'activity_3'

        activities[activity_1.name] = activity_1.__dict__
        activities[activity_2.name] = activity_2.__dict__
        activities[activity_3.name] = activity_3.__dict__

        blacklist = ['activity_1', 'activity_2']

        blackListFilter = BlackListFilter(blacklist, activities)
        filtered_list = blackListFilter.filter_by_black_list()

        # It'd be nice to test weather the activity left in the filtered list
        # is indeed activity_3, but using a dict is cumbersome.

        self.assertEquals(1, len(filtered_list))
