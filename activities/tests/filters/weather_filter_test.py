import unittest
from fun_genie.classes.filters.weather_filter import WeatherFilter
from fun_genie.classes.services.location_service import LocationService

class WeatherFilterTest(unittest.TestCase):
  def testGetWeatherInfo(self):
    latitude = "9.9067790"
    longitude = "-83.9876530"
    location_service = LocationService(latitude,longitude)
    self.assertIsNotNone(location_service.get_weather_info())

  def testWithIndoorActivity(self):
    activity_hash = {'Go online and play games': {"end_time": None, "name": "Go online and play games",
                                                  "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                  "min_wind_speed": float("NaN"),
                                                  "acceptable_weather_conditions": None,
                                                  "url": "https://www.google.com/search?q=\"online+games\"",
                                                  "start_time": None, "unacceptable_clouds_code": None,
                                                  "weights": [0.0, 10.0, 0.0, 1.0, 9.0, 5.0, float("NaN"),
                                                              float("NaN"), float("NaN"), float("NaN"),
                                                              float("NaN"),
                                                              float("NaN"), 5.0, float("NaN"), float("NaN"),
                                                              float("NaN"), float("NaN"), 8.0, float("NaN"),
                                                              float("NaN"), float("NaN"), 0.0, 10.0, 6.0,
                                                              float("NaN"), float("NaN"), float("NaN"), 9.0,
                                                              float("NaN"), 3.0, 10.0, 0.0],
                                                  "min_temp": float("NaN")}}

    weather_mock = {"weatherObservation": {"elevation": 994, "lng": -84.15,
                                           "observation": "MRPV 242009Z 30006KT 4000 -RA BR BKN002 20/19 A2994 NOSIG",
                                           "ICAO": "MRPV", "clouds": "broken clouds", "dewPoint": "19",
                                           "cloudsCode": "BKN", "datetime": "2016-11-24 20:09:00",
                                           "countryCode": "CR", "temperature": "20", "humidity": 93,
                                           "stationName": "Tobias Bolanos International",
                                           "weatherCondition": "n/a", "windDirection": 300, "windSpeed": "06",
                                           "lat": 9.95}}

    weather_filter = WeatherFilter(activity_hash, weather_mock["weatherObservation"])
    result = weather_filter.filter_activity_by_weather('Go online and play games')
    self.assertEqual(True, result)

  def testWithOutdoorActivity(self):
    activity_hash = {'Go fly a kite': {"end_time": "SS-1", "name": "Go fly a kite", "max_temp": float("NaN"),
                                       "max_wind_speed": 15.0, "min_wind_speed": 4.0,
                                       "acceptable_weather_conditions": "n/a or ",
                                       "url": "https://maps.google.com/maps?sll=%F&q=\"bike+trail\"",
                                       "start_time": "SR+3", "unacceptable_clouds_code": None,
                                       "weights": [6.0, 4.0, 0.0, 10.0, 4.0, 0.0, float("NaN"), float("NaN"),
                                                   float("NaN"), 10.0, 0.0, float("NaN"), float("NaN"),
                                                   float("NaN"),
                                                   float("NaN"), float("NaN"), float("NaN"), 5.0, float("NaN"), 6.0,
                                                   float("NaN"), 10.0, 0.0, float("NaN"), 6.0, 4.0, float("NaN"),
                                                   float("NaN"), float("NaN"), 2.0, 0.0, 10.0], "min_temp": 10.0}}

    weather_mock = {"weatherObservation":{"elevation":994,"lng":-84.15,"observation":"MRPV 242009Z 30006KT 4000 -RA BR BKN002 20/19 A2994 NOSIG",
                                          "ICAO":"MRPV","clouds":"broken clouds","dewPoint":"19","cloudsCode":"BKN","datetime":"2016-11-24 20:09:00",
                                          "countryCode":"CR","temperature":"20","humidity":93,"stationName":"Tobias Bolanos International",
                                          "weatherCondition":"n/a","windDirection":300,"windSpeed":"06","lat":9.95}}

    weather_filter = WeatherFilter(activity_hash, weather_mock["weatherObservation"])
    result = weather_filter.filter_activity_by_weather('Go fly a kite')
    self.assertEqual(False, result)

  def testFilterAllTheActivitiesWithOne(self):
    activity_hash = {'Go online and play games': {"end_time": None, "name": "Go online and play games",
                                                  "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                  "min_wind_speed": float("NaN"),
                                                  "acceptable_weather_conditions": None,
                                                  "url": "https://www.google.com/search?q=\"online+games\"",
                                                  "start_time": None, "unacceptable_clouds_code": None,
                                                  "weights": [0.0, 10.0, 0.0, 1.0, 9.0, 5.0, float("NaN"),
                                                              float("NaN"), float("NaN"), float("NaN"),
                                                              float("NaN"),
                                                              float("NaN"), 5.0, float("NaN"), float("NaN"),
                                                              float("NaN"), float("NaN"), 8.0, float("NaN"),
                                                              float("NaN"), float("NaN"), 0.0, 10.0, 6.0,
                                                              float("NaN"), float("NaN"), float("NaN"), 9.0,
                                                              float("NaN"), 3.0, 10.0, 0.0],
                                                  "min_temp": float("NaN")}}

    weather_mock = {"weatherObservation": {"elevation": 994, "lng": -84.15,
                                           "observation": "MRPV 242009Z 30006KT 4000 -RA BR BKN002 20/19 A2994 NOSIG",
                                           "ICAO": "MRPV", "clouds": "broken clouds", "dewPoint": "19",
                                           "cloudsCode": "BKN", "datetime": "2016-11-24 20:09:00",
                                           "countryCode": "CR", "temperature": "20", "humidity": 93,
                                           "stationName": "Tobias Bolanos International",
                                           "weatherCondition": "n/a", "windDirection": 300, "windSpeed": "06",
                                           "lat": 9.95}}

    weather_filter = WeatherFilter(activity_hash, weather_mock["weatherObservation"])
    result = weather_filter.filter_activities_by_weather()
    self.assertEqual(1, len(result))

  def testFilterAllActivitiesWithTwo(self):
    activity_hash = {'Go online and play games': {"end_time": None, "name": "Go online and play games",
                                                  "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                  "min_wind_speed": float("NaN"),
                                                  "acceptable_weather_conditions": None,
                                                  "url": "https://www.google.com/search?q=\"online+games\"",
                                                  "start_time": None, "unacceptable_clouds_code": None,
                                                  "weights": [0.0, 10.0, 0.0, 1.0, 9.0, 5.0, float("NaN"),
                                                              float("NaN"), float("NaN"), float("NaN"),
                                                              float("NaN"),
                                                              float("NaN"), 5.0, float("NaN"), float("NaN"),
                                                              float("NaN"), float("NaN"), 8.0, float("NaN"),
                                                              float("NaN"), float("NaN"), 0.0, 10.0, 6.0,
                                                              float("NaN"), float("NaN"), float("NaN"), 9.0,
                                                              float("NaN"), 3.0, 10.0, 0.0],
                                                  "min_temp": float("NaN")},
                     'Go fly a kite': {"end_time": "SS-1", "name": "Go fly a kite", "max_temp": float("NaN"),
                                       "max_wind_speed": 15.0, "min_wind_speed": 4.0,
                                       "acceptable_weather_conditions": "n/a or ",
                                       "url": "https://maps.google.com/maps?sll=%F&q=\"bike+trail\"",
                                       "start_time": "SR+3", "unacceptable_clouds_code": None,
                                       "weights": [6.0, 4.0, 0.0, 10.0, 4.0, 0.0, float("NaN"), float("NaN"),
                                                   float("NaN"), 10.0, 0.0, float("NaN"), float("NaN"),
                                                   float("NaN"),
                                                   float("NaN"), float("NaN"), float("NaN"), 5.0, float("NaN"), 6.0,
                                                   float("NaN"), 10.0, 0.0, float("NaN"), 6.0, 4.0, float("NaN"),
                                                   float("NaN"), float("NaN"), 2.0, 0.0, 10.0], "min_temp": 10.0}}

    weather_mock = {"weatherObservation": {"elevation": 994, "lng": -84.15,
                                           "observation": "MRPV 242009Z 30006KT 4000 -RA BR BKN002 20/19 A2994 NOSIG",
                                           "ICAO": "MRPV", "clouds": "broken clouds", "dewPoint": "19",
                                           "cloudsCode": "BKN", "datetime": "2016-11-24 20:09:00",
                                           "countryCode": "CR", "temperature": "20", "humidity": 93,
                                           "stationName": "Tobias Bolanos International",
                                           "weatherCondition": "n/a", "windDirection": 300, "windSpeed": "06",
                                           "lat": 9.95}}

    weather_filter = WeatherFilter(activity_hash, weather_mock["weatherObservation"])
    result = weather_filter.filter_activities_by_weather()
    self.assertEqual(1, len(result))
