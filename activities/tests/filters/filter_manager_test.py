import unittest
from fun_genie.classes.filters.filter_manager import FilterManager



class FilterManagerTest(unittest.TestCase):
  def test_apply_filters_without_location(self):
    filter_manager = FilterManager()
    activity_hash = {}
    activity_hash['Go whitewater rafting'] = {"end_time": "SS-1", "name": "Go whitewater rafting ",
                                              "max_temp": float("NaN"), "max_wind_speed": 10.0,
                                              "min_wind_speed": 0.0, "acceptable_weather_conditions": "n/a or ",
                                              "url": "https://maps.google.com/maps?sll=%F&q=\"whitewater+rafting\"",
                                              "start_time": "SR+3", "unacceptable_clouds_code": None,
                                              "weights": [8.0, 2.0, 0.0, 2.0, 8.0, 4.0, float("NaN"), float("NaN"),
                                                          float("NaN"), 10.0, 0.0, float("NaN"), float("NaN"),
                                                          float("NaN"), float("NaN"), float("NaN"), 4.0, 5.0,
                                                          float("NaN"), 10.0, float("NaN"), 10.0, 0.0, 3.0, 8.0,
                                                          float("NaN"), float("NaN"), float("NaN"), 10.0, 4.0, 0.0,
                                                          10.0], "min_temp": 21.0}
    activity_hash['Go online and play games'] = {"end_time": None, "name": "Go online and play games",
                                                 "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                 "min_wind_speed": float("NaN"),
                                                 "acceptable_weather_conditions": None,
                                                 "url": "https://www.google.com/search?q=\"online+games\"",
                                                 "start_time": None, "unacceptable_clouds_code": None,
                                                 "weights": [0.0, 10.0, 0.0, 1.0, 9.0, 5.0, float("NaN"),
                                                             float("NaN"), float("NaN"), float("NaN"), float("NaN"),
                                                             float("NaN"), 5.0, float("NaN"), float("NaN"),
                                                             float("NaN"), float("NaN"), 8.0, float("NaN"),
                                                             float("NaN"), float("NaN"), 0.0, 10.0, 6.0,
                                                             float("NaN"), float("NaN"), float("NaN"), 9.0,
                                                             float("NaN"), 3.0, 10.0, 0.0],
                                                 "min_temp": float("NaN")}
    black_list = ['Go whitewater rafting']
    filtered_list = filter_manager.apply_filters(activity_hash, black_list)
    activity = filtered_list[0]
    self.assertIsNotNone(activity)

  def test_apply_filters_with_location(self):
    filter_manager = FilterManager()
    activity_hash = {}
    activity_hash['Go whitewater rafting'] = {"end_time": "SS-1", "name": "Go whitewater rafting ",
                                              "max_temp": float("NaN"), "max_wind_speed": 10.0,
                                              "min_wind_speed": 0.0, "acceptable_weather_conditions": "n/a or ",
                                              "url": "https://maps.google.com/maps?sll=%F&q=\"whitewater+rafting\"",
                                              "start_time": "SR+3", "unacceptable_clouds_code": None,
                                              "weights": [8.0, 2.0, 0.0, 2.0, 8.0, 4.0, float("NaN"), float("NaN"),
                                                          float("NaN"), 10.0, 0.0, float("NaN"), float("NaN"),
                                                          float("NaN"), float("NaN"), float("NaN"), 4.0, 5.0,
                                                          float("NaN"), 10.0, float("NaN"), 10.0, 0.0, 3.0, 8.0,
                                                          float("NaN"), float("NaN"), float("NaN"), 10.0, 4.0, 0.0,
                                                          10.0], "min_temp": 21.0}
    activity_hash['Go online and play games'] = {"end_time": None, "name": "Go online and play games",
                                                 "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                 "min_wind_speed": float("NaN"),
                                                 "acceptable_weather_conditions": None,
                                                 "url": "https://www.google.com/search?q=\"online+games\"",
                                                 "start_time": None, "unacceptable_clouds_code": None,
                                                 "weights": [0.0, 10.0, 0.0, 1.0, 9.0, 5.0, float("NaN"),
                                                             float("NaN"), float("NaN"), float("NaN"), float("NaN"),
                                                             float("NaN"), 5.0, float("NaN"), float("NaN"),
                                                             float("NaN"), float("NaN"), 8.0, float("NaN"),
                                                             float("NaN"), float("NaN"), 0.0, 10.0, 6.0,
                                                             float("NaN"), float("NaN"), float("NaN"), 9.0,
                                                             float("NaN"), 3.0, 10.0, 0.0],
                                                 "min_temp": float("NaN")}

    black_list = []
    weather_mock = {"weatherObservation": {"elevation": 994, "lng": -84.15,
                                           "observation": "MRPV 242009Z 30006KT 4000 -RA BR BKN002 20/19 A2994 NOSIG",
                                           "ICAO": "MRPV", "clouds": "broken clouds", "dewPoint": "19",
                                           "cloudsCode": "BKN", "datetime": "2016-11-24 20:09:00",
                                           "countryCode": "CR", "temperature": "20", "humidity": 93,
                                           "stationName": "Tobias Bolanos International",
                                           "weatherCondition": "n/a", "windDirection": 300, "windSpeed": "06",
                                           "lat": 9.95}}
    timeJson_mock = {
      'sunset': '2016-06-13 17:57',
      'sunrise': '2016-06-13 05:15',
      'time': '2016-06-13 13:05'
    }
    filtered_list = filter_manager.apply_filters(activity_hash, black_list, timeJson_mock, weather_mock["weatherObservation"]);
    self.assertIsNotNone(filtered_list)

  def test_apply_filters_with_location_SS(self):
    filter_manager = FilterManager()
    black_list = []
    activity_hash = {'Go online and play games': {"end_time": None, "name": "Go online and play games",
                                                  "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                  "min_wind_speed": float("NaN"),
                                                  "acceptable_weather_conditions": None,
                                                  "url": "https://www.google.com/search?q=\"online+games\"",
                                                  "start_time": None, "unacceptable_clouds_code": None,
                                                  "weights": [0.0, 10.0, 0.0, 1.0, 9.0, 5.0, float("NaN"),
                                                              float("NaN"), float("NaN"), float("NaN"),
                                                              float("NaN"),
                                                              float("NaN"), 5.0, float("NaN"), float("NaN"),
                                                              float("NaN"), float("NaN"), 8.0, float("NaN"),
                                                              float("NaN"), float("NaN"), 0.0, 10.0, 6.0,
                                                              float("NaN"), float("NaN"), float("NaN"), 9.0,
                                                              float("NaN"), 3.0, 10.0, 0.0],
                                                  "min_temp": float("NaN")},
                     "Watch the sunset": {"end_time": "SS", "name": "Watch the sunset", "max_temp": float("NaN"),
                                          "max_wind_speed": 10.0, "min_wind_speed": 0.0,
                                          "acceptable_weather_conditions": "n/a or ", "url": None, "start_time": "SS-1",
                                          "unacceptable_clouds_code": "not OVC",
                                          "weights": [2.0, 8.0, 0.0, 2.0, 3.0, 3.0, float("NaN"), float("NaN"),
                                                      float("NaN"),
                                                      3.0, 8.0, float("NaN"), float("NaN"), float("NaN"), float("NaN"),
                                                      float("NaN"), 0.0, float("NaN"), float("NaN"), 5.0, float("NaN"),
                                                      10.0, 0.0, 8.0,
                                                      3.0, float("NaN"), float("NaN"), 6.0, 6.0, 1.0, 0.0, 10.0],
                                          "min_temp": 15.0},
                     "Go to a restaurant with friends": {"end_time": "SS+4", "name": "Go to a restaurant with friends",
                                                         "max_temp": float("NaN"), "max_wind_speed": float("NaN"),
                                                         "min_wind_speed": float("NaN"),
                                                         "acceptable_weather_conditions": None,
                                                         "url": "https://maps.google.com/maps?sll=%F&q=\"restaurant\"",
                                                         "start_time": "SR+4", "unacceptable_clouds_code": None,
                                                         "weights": [2.0, 8.0, 0.0, 1.0, 9.0, 8.0, float("NaN"),
                                                                     float("NaN"), float("NaN"), float("NaN"),
                                                                     float("NaN"), float("NaN"), float("NaN"),
                                                                     float("NaN"), float("NaN"), 8.0, 7.0, 1.0,
                                                                     float("NaN"),
                                                                     float("NaN"), 3.0, 3.0, 7.0, float("NaN"), 4.0,
                                                                     float("NaN"), float("NaN"), float("NaN"), 9.0, 3.0,
                                                                     0.0, 10.0], "min_temp": float("NaN")}}
    weather_mock = {"weatherObservation": {"elevation": 994, "lng": -84.15,
                                           "observation": "MRPV 242009Z 30006KT 4000 -RA BR BKN002 20/19 A2994 NOSIG",
                                           "ICAO": "MRPV", "clouds": "broken clouds", "dewPoint": "19",
                                           "cloudsCode": "BKN", "datetime": "2016-11-24 20:09:00",
                                           "countryCode": "CR", "temperature": "20", "humidity": 93,
                                           "stationName": "Tobias Bolanos International",
                                           "weatherCondition": "n/a", "windDirection": 300, "windSpeed": "06",
                                           "lat": 9.95}}
    timeJson_mock = {
      'sunset': '2016-06-13 17:57',
      'sunrise': '2016-06-13 05:15',
      'time': '2016-06-13 13:05'
    }
    filtered_list = filter_manager.apply_filters(activity_hash, black_list, timeJson_mock, weather_mock["weatherObservation"]);
    self.assertIsNotNone(filtered_list)
