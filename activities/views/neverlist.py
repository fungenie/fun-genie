import logging

from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from ..constants import NEVERLIST

logger = logging.getLogger(__name__)

def index(request):
    neverlist = request.session.get(NEVERLIST, list())
    if not neverlist:
        title = 'You Have No Never Activities'
    else:
        title = 'List of "Never" Activities'
    return render(request, 'activities/neverlist.html', { 'neverlist': neverlist, 'title': title })

@csrf_exempt
def delete(request):
    if request.method == 'POST':
        try:
            neverlist = request.session.pop(NEVERLIST, list())
            neverlist.pop(int(request.POST['index']))
            request.session[NEVERLIST] = neverlist
        except (IndexError, ValueError) as ex:
            logger.error(ex)
        finally:
            return HttpResponseRedirect(reverse('neverlist'))
    else:
        return HttpResponseForbidden()
