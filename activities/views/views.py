import Cookie
import json
import logging
import random
from datetime import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render, render_to_response
from django.urls import reverse

from ..classes.activity_suggestion_engine import ActivitySuggestionEngine
from ..classes.services.location_service import LocationService
from ..constants import NEVERLIST, VOTES
from ..exceptions import NoPossibleActivitiesError
from ..forms import ContactForm

logger = logging.getLogger(__name__)

def home(request):
  if 'last_visit' in request.COOKIES:
    return init_timezone(request)
  else:
    return render_to_response('activities/home.html')

def init_timezone(request):
    return render(request, 'activities/init_timezone.html')

def activities(request, vote=None):
  votes = request.session.pop(VOTES, dict())
  neverlist = request.session.pop(NEVERLIST, list())

  ## We add the vote to the votes list
  if vote:
    votes[vote['activity']] = vote['value']

  ## We setup the latitude and longitude values.
  latitude = request.COOKIES['posLat'] if 'posLat' in request.COOKIES else None
  longitude = request.COOKIES['posLong'] if 'posLong' in request.COOKIES else None
  weatherJSON = None
  timeJSON = None

  ## At this point we should have the latitude and longitude of the user from an js[geolocation.js].
  if latitude and longitude:
    locationService = LocationService(latitude, longitude)
    if 'weatherJSON' not in request.COOKIES or 'timeJSON' not in request.COOKIES:
      timeJSON = locationService.get_time_data_from_api()
      weatherJSON = locationService.get_weather_info()
    else:
      timeJSON = json.loads(request.COOKIES['timeJSON'])
      weatherJSON = json.loads(request.COOKIES['weatherJSON'])

  """ Every time a user votes, a new 'ActivitySuggestionEngine' instance
  is born, which processes the whole vote_list over and over again (see
  the made_vote function called in its constructor) which might
  eventually lead to performance issues.

  Another problem is that votes is not kept up to date, and will
  contain blacklisted items even tho they're in a separate list
  (blacklist). This calls for a major refactor. """

  # TO-DO: Investigate why are we doing this if.
  if weatherJSON:
    engine = ActivitySuggestionEngine(votes, neverlist, timeJSON, weatherJSON['weatherObservation'])
  else:
    engine = ActivitySuggestionEngine(votes, neverlist, timeJSON, weatherJSON)

  try:
      suggestion = engine.compute_suggestion(votes)
  except NoPossibleActivitiesError:
    return render(request, 'activities/no_activities.html')

  if vote:
    if vote['value'] == -2 and vote['activity'] not in neverlist:
      neverlist.append(vote['activity'])

  if suggestion['name'] == 'Sorry, Genie ran out of ideas!':
    return render(request, 'activities/no_activities.html')

  images = ['results_genie.png', 'run_out_of_ideas_genie.png', 'thinking_genie.png']
  display_image = images[random.randint(0, 2)]

  if suggestion['url']:
    suggestion['url']

  logger = logging.getLogger('fun-genie')
  logger.info('Activity name: %s | Activity start_time: %s | Activity end_time: %s | Current Sunrise: %s | Current Sunset: %s | Current Time: %s',
          suggestion['name'], suggestion['start_time'], suggestion['end_time'], timeJSON['sunrise'], timeJSON['sunset'], timeJSON['time'])

  response = render_to_response('activities/activities.html', {'activity': suggestion['name'], 'url': suggestion['url'],
                                                    'genie_image': display_image})

  if 'weatherJSON' not in request.COOKIES or 'timeJSON' not in request.COOKIES:
    if timeJSON is not None and weatherJSON is not None:
      response.set_cookie('timeJSON', json.dumps(timeJSON))
      response.set_cookie('weatherJSON', json.dumps(weatherJSON))

  if 'last_visit' in request.COOKIES:
    last_visit = request.COOKIES['last_visit']
  else:
    response.set_cookie('last_visit', datetime.now())
    response.set_cookie('use_time_and_weather', False)

  request.session[VOTES] = votes
  request.session[NEVERLIST] = neverlist

  return response

# GETs shouldn't modify the state of the application
# vote should be a POST
def vote(request, activity, vote):
  new_vote = {'activity': activity, 'value': int(vote)}
  return activities(request, new_vote)

def preferences(request):
  try:
    time_and_weather_flag = request.COOKIES['use_time_and_weather']
    if time_and_weather_flag:
      on_class = 'btn-success'
      off_class = 'btn-default'
    else:
      on_class = 'btn-default'
      off_class = 'btn-danger'
  except (Cookie.CookieError, KeyError):
    on_class = 'btn-default'
    off_class = 'btn-danger'

  return render_to_response('activities/preferences.html', {'on_button_class': on_class, 'off_button_class': off_class})

def noactivities(request):
  return render_to_response('no_activities.html')

def reset_votes(request):
    request.session[VOTES] = dict()
    return HttpResponseRedirect(reverse('activities_root'))

def bye_session(request):
  return render(request, 'activities/bye_session.html')


def suggest_activity(request):
  return render_to_response('suggest_activity.html')
