from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from ..forms import ContactForm


def index(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            send_mail(subject, message, settings.EMAIL_FROM_ADDRS, [settings.EMAIL_TO_ADDRS])
            return HttpResponseRedirect(reverse('contact_thankyou'))
    else:
        form = ContactForm()

    return render(request, 'activities/contact.html', {'form': form})

def thankyou(request):
    return render(request, 'activities/thankyou.html')
