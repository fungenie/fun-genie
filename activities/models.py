from django.contrib.postgres.fields import ArrayField
from django.db import models


# TextField is used without discretion, while it should be a CharField
# with a well determined max_length.
#
# To do: Figure out a size for each varchar field.

class Activity(models.Model):
    name = models.TextField(unique=True)
    url = models.TextField(blank=True, null=True)
    acceptable_weather_conditions = models.TextField(blank=True, null=True)
    unacceptable_clouds_code = models.TextField(blank=True, null=True)
    min_wind_speed = models.FloatField()
    max_wind_speed = models.FloatField()
    min_temp = models.FloatField()
    max_temp = models.FloatField()
    start_time = models.TextField(blank=True, null=True)
    end_time = models.TextField(blank=True, null=True)
    weights = ArrayField(models.FloatField())

    def __str__(self):
        return self.name


    class Meta:
        verbose_name_plural = "activities"
